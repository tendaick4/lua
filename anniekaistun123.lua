   Config =
       Config or
       {
           ["Misc - Fps Boost"] = false,
           ["Misc - Screen Video"] = false,
           ["Misc - Lock Devil Fruit"] = {"Dough", "Light", "Spirit", "Kitsune", "Meme"},
           ["Misc - Snipe Fruit"] = {"Dough"},
           ["Misc - Default Raid Type"] = "Dark"
       }
   
   (function()
       if Config["Misc - Fps Boost"] then
           game:GetService("RunService"):Set3dRenderingEnabled(0)
           if not Config["Misc - Screen Video"] then
               return
           end
           if not isfile(".huhuhuhu.webm") then
               writefile(".huhuhuhu.webm", request({Url = Config["Misc - Screen Video"], Method = "GET"}).Body)
           end
           repeat
               wait()
           until getcustomasset(".huhuhuhu.webm")
           local ScreenGui = Instance.new("ScreenGui")
           ScreenGui.IgnoreGuiInset = true
   
           local VideoFrame = Instance.new("VideoFrame")
           local UICorner = Instance.new("UICorner")
   
           ScreenGui.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")
           ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Global
           ScreenGui.Enabled = true
   
           VideoFrame.Parent = ScreenGui
           VideoFrame.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
           VideoFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
           VideoFrame.BorderSizePixel = 0
           VideoFrame.Position = UDim2.new(0, 0, -0.050999999, 0)
           VideoFrame.Size = UDim2.new(1, 0, 1.04999995, 0)
           VideoFrame.Playing = true
           VideoFrame.Looped = true
           VideoFrame.Volume = 10.000
           VideoFrame.Video = getcustomasset(".huhuhuhu.webm")
           VideoFrame.ZIndex = 50
           UICorner.Parent = VideoFrame
           game:GetService "TeleportService":SetTeleportGui(ScreenGui)
       end
   end)()
   Start_Time = os.time()
  
   MainGui = Instance.new("ScreenGui")
   MainFrame = Instance.new("Frame")
   MainFrame.Visible = true
   Hub = Instance.new("TextLabel")
   UIStroke = Instance.new("UIStroke")
   Panel = Instance.new("TextLabel")
   Panel2 = Instance.new("TextLabel")
   UIGradient = Instance.new("UIGradient")
   
   MainGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
   MainGui.Name = "MainGui"
   MainGui.Parent = game:GetService("CoreGui")
   --game:GetService"TeleportService":SetTeleportGui(MainGui)
   
   MainFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
   MainFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
   MainFrame.BorderSizePixel = 0
   MainFrame.Size = UDim2.new(1, 0, 1, 0)
   MainFrame.Name = "MainFrame"
   MainFrame.Parent = MainGui
   MainFrame.BackgroundTransparency = 1
   Hub.Font = Enum.Font.GothamBold
   Hub.Text = ""
   Hub.TextColor3 = Color3.fromRGB(80.00000283122063, 80.00000283122063, 80.00000283122063)
   Hub.TextSize = 30
   Hub.AnchorPoint = Vector2.new(0.5, 0.5)
   Hub.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
   Hub.BackgroundTransparency = 1
   Hub.BorderColor3 = Color3.fromRGB(0, 0, 0)
   Hub.BorderSizePixel = 0
   Hub.Position = UDim2.new(0.5, 0, 0.465000004, 0)
   Hub.Size = UDim2.new(1, 0, 0, 20)
   Hub.Name = "Hub"
   Hub.Parent = MainFrame
   Hub.Visible = false
   
   UIStroke.Color = Color3.fromRGB(80.00000283122063, 80.00000283122063, 80.00000283122063)
   UIStroke.Thickness = 0.5
   UIStroke.Parent = Hub
   
   Panel.Font = Enum.Font.GothamBold
   Panel.Text = "TEST HUB KAITUN"
   Panel.TextColor3 = Color3.fromRGB(80.00000283122063, 80.00000283122063, 80.00000283122063)
   Panel.TextSize = 50
   Panel.AnchorPoint = Vector2.new(0.5, 0.5)
   Panel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
   Panel.BackgroundTransparency = 1
   Panel.BorderColor3 = Color3.fromRGB(0, 0, 0)
   Panel.BorderSizePixel = 0
   Panel.Position = UDim2.new(0.5, 0, 0.5, 0)
   Panel.Size = UDim2.new(0, 200, 0, 50)
   Panel.Name = "Panel"
   Panel.Parent = MainFrame
   
   Panel2.Font = Enum.Font.GothamBold
   Panel2.Text = ""
   Panel2.TextColor3 = Color3.fromRGB(80.00000283122063, 80.00000283122063, 80.00000283122063)
   Panel2.TextSize = 14
   Panel2.AnchorPoint = Vector2.new(0.5, 0.5)
   Panel2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
   Panel2.BackgroundTransparency = 1
   Panel2.BorderColor3 = Color3.fromRGB(0, 0, 0)
   Panel2.BorderSizePixel = 0
   Panel2.Position = UDim2.new(0.5, 0, 0.9124999976, 0)
   Panel2.Size = UDim2.new(1, 0, 0, 20)
   Panel2.Name = "Panel"
   Panel2.Parent = MainFrame
  
   -- main
   
   repeat
       task.wait()
       if
           game:GetService("Players").LocalPlayer.PlayerGui:WaitForChild("Main"):FindFirstChild "ChooseTeam" and
               game:GetService("Players").LocalPlayer.PlayerGui:WaitForChild("Main").ChooseTeam.Visible == true
        then
           for i, v in pairs(
               getconnections(
                   game:GetService("Players").LocalPlayer.PlayerGui.Main.ChooseTeam.Container.Pirates.Frame.TextButton.Activated
               )
           ) do
               for a, b in pairs(getconnections(game:GetService("UserInputService").TouchTapInWorld)) do
                   b:Fire()
               end
               v.Function()
           end
       end
   until game.Players.LocalPlayer.Team ~= nil and game:IsLoaded()
   --LocalPlayer.CameraMaxZoomDistance = 250
   print("run")
   OriginalName = {}
   OriginalName2 = {}
   fruitprice = {}
            for name, _ in pairs(require(game.ReplicatedStorage.Shop).Items) do
               
               local parts = {}
               for part in string.gmatch(name, "[^-]+") do
                  table.insert(parts, part)
               end
               local key = table.concat(parts, "-")
               local value = parts[1] .. " Fruit"
               OriginalName2[value] = key
               OriginalName[key] = value
              
            end
   
   function GetOriginalName(uwu) 
      
   end 
   
   wait()
   local Players = game.Players
   
   local LocalPlayer = Players.LocalPlayer
   
   local Character = LocalPlayer.Character
   
   local Humanoid = Character:WaitForChild("Humanoid")
   
   local HumanoidRootPart = Character:WaitForChild("HumanoidRootPart")
   
   local TweenService = game:GetService("TweenService")
   
   local Quests = require(game.ReplicatedStorage.Quests)
   
   getgenv().temporary = {}
   
   relmon = {
       ["Gorilla King"] = "The Gorilla King",
       ["Mil. Soldier"] = "Military Soldier",
       ["Mil. Spy"] = "Military Spy",
       ["Swan's Pirate"] = "Swan Pirate"
   }
   
   local ids = {
       ""
   }
   
   MeleeConverter = {
       ["Black Leg"] = "BlackLeg",
       ["Fishman Karate"] = "FishmanKarate",
       ["Electro"] = "Electro",
       ["Dragon Claw"] = "DragonClaw",
       ["Superhuman"] = "Superhuman",
       ["Sharkman Karate"] = "SharkmanKarate",
       ["Death Step"] = "DeathStep",
       ["Dragon Talon"] = "DragonTalon",
       ["Godhuman"] = "Godhuman",
       ["Electric Claw"] = "ElectricClaw"
   }
   
   Melees = {
       "DragonTalon",
       "Superhuman",
       "Godhuman",
       "ElectricClaw",
       "SharkmanKarate",
       "DeathStep",
       "BlackLeg",
       "FishmanKarate",
       "Electro",
       "DragonClaw"
   }
   rua = {}
   -- $loadstr
   
   local fatk = loadstring(game:HttpGet("https://raw.githubusercontent.com/Dextral-Code/pet99/main/123123"))()
   
   getgenv().Time = {}
   Time.__index = Time
   function Time.new(name)
       obj =
           {
               name = name,
               createtime = os.time(),
               keyframes = {},
               a = workspace
           },
           setmetatable(obj, Time)
   end
   
   function Time:CreateKeyframe(name)
       self.keyframes[name] = {Start = os.time()}
   end
   
   function Time:CaculateElapsed()
       return os.time() - self.createtime
   end
   
   function Time:Reset()
       self.createtime = os.time()
   end
   
   function Time:CaculateElapsedFromKeyFrames(name)
       return os.time() - self.keyframes[name]
   end
   getgenv().Tasks = {}
   Tasks.__index = Tasks
   
   function Tasks.new(name, func, autoStart, level)
       local obj = {
           Name = name,
           Function = func,
           NameTask = name,
           AutoStart = autoStart or function()
                   return false
               end,
           Level = level
       }
   
       setmetatable(obj, Tasks)
       return obj
   end
   function Tasks:Prepare()
       self.Loop =
           task.spawn(
           function()
               while wait(.7) do
                   if not self:IsRunning() then
                       if TaskHandler and self.AutoStart() then
                           local runningtasks = TaskHandler:GetRunningTasks(self.NameTask)
                           cancelhhh = false
                           for i, v in pairs(runningtasks) do
                               if v.Level > self.Level and v.NameTask ~= "background" then
                                   print(v.NameTask)
                                   cancelhhh = true
                               end
                           end
                           if not cancelhhh or self.NameTask == "background" then
                               self.Thread = task.spawn(self.Function)
                               wait()
                           end
                       else
                           self:Stop()
                       end
                   end
               end
           end
       )
   end
   
   function Tasks:Stop()
       if self.NameTask == "background" then
           return
       end
       if not self:IsRunning() then
           return
       end
       if self.Thread then
           task.cancel(self.Thread)
       end
       if self.ForceStoped then
           self.ForceStoped()
       end
   end
   function Tasks:Start()
       if self:IsRunning() or not self.AutoStart() then
           return
       end
       self.Thread = task.spawn(self.Function)
   
       return self
   end
   
   function Tasks:IsRunning()
       if not self.Thread then
           return false
       end
       return coroutine.status(self.Thread) ~= "dead"
   end
   
   if not getgenv().ListTask then
       getgenv().ListTask = {}
   end
   
   getgenv().TaskHandler = {}
   
   function TaskHandler:AddTask(Name, func, mm, level)
       ListTask[Name] = Tasks.new(name, func, mm, level)
       return ListTask[Name]
   end
   
   function TaskHandler:StopTask(Name)
       if not ListTask[Name] then
           return
       end
       ListTask[Name]:Stop()
   end
   
   function TaskHandler:CancelAll(c)
       for k, v in pairs(ListTask) do
           if k ~= c and k ~= "background" then
               warn("stopped", k)
               v:Stop()
           end
       end
   end
   
   function TaskHandler:GetRunningTasks(Config)
       Config = Config or {Whitelist = {}}
       if type(Config.Whitelist) == "string" then
          
           Config.Whitelist = {Config.Whitelist}
       end
   
       local data = {}
       for i, v in pairs(ListTask) do
           if v:IsRunning() and not table.find(Config.Whitelist, i) and i ~= "background" then
              print(i)
               data[i] = v
           end
       end
       return data
   end
   
   function TaskHandler:PrimaryTask(name)
       local a0 = TaskHandler:GetRunningTasks({Whitelist = name})
       wait(.5)
       local a1 = TaskHandler:GetRunningTasks({Whitelist = name})
       return #a0 < 1 and #a1 < 1
   end
   
   TaskHandler:CancelAll()
   -- usage
   
   getgenv().QuestLib = {}
   
   function QuestLib:getQuestPath()
       repeat
           wait()
       until LocalPlayer.Team ~= nil
       return game:GetService("Players").LocalPlayer.PlayerGui.Main.Quest
   end
   
   function QuestLib:isClaimed()
       local Quest = QuestLib:getQuestPath()
       return Quest.Visible
   end
   
   function QuestLib:getNameQuest()
       return string.gsub(
           string.gsub(self:getQuestPath().Container.QuestTitle.Title.Text, "Defeat ", ""),
           " %p(./.)%p",
           ""
       )
   end
   
   function QuestLib:getNameQuestButUpdated()
       local orig =
           string.gsub(
           string.gsub(QuestLib:getQuestPath().Container.QuestTitle.Title.Text, "Defeat .", ""),
           " %p(./.)%p",
           ""
       )
       local change1 = checkIsNameMonNotMatch(orig)
       if relmon[change1] then
           change1 = relmon[change1]
       end
       return change1
   end
   
   function QuestLib:Abandon()
       CommF("AbandonQuest")
   end
   
   function QuestLib:StartQuest(quest)
      
       CommF("StartQuest", quest.NameQuest, quest.LevelQuest)
   end
   
   -- $func
   
   function lockcam(lookAtPos)
       local camera = game.Workspace.CurrentCamera
       local cameraPos = camera.CFrame.Position
       local newLookAt = CFrame.new(cameraPos, lookAtPos)
       camera.CFrame = newLookAt
   end
   
   function CommF(...)
       return game.ReplicatedStorage.Remotes.CommF_:InvokeServer(...)
   end
   
   function await(fn, ...)
       repeat
           wait()
           cums = fn(unpack({...}))
           if not cums then
               wait(1)
               return await(fn, ...)
           end
       until cums
   end
   
   function StartQuest(questdata)
       --setclipboard (type(questdata))
       if type(questdata) ~= "table" then
           return true
       end
   
       if QuestLib:isClaimed() then
           if not string.find(QuestLib:getNameQuest(), (questdata.NameMon)) then
              print("Abandon2")
               QuestLib:Abandon()
           end
           return false
       end
       local NPCpos = QuestPoint[questdata.NameQuest]
       if not NPCpos then
           return false
       end
       if QuestLib:isClaimed() and QuestLib.getNameQuestButUpdated() == QuestData.NameMon then
           return false
       end
       if CaculateDist(NPCpos) < 6 then
           to(NPCpos)
           wait(1)
           QuestLib:StartQuest(questdata)
           OldQuest = questdata
           return true
       end
   
       to(NPCpos)
   end
   
   function CFrameQuest()
       QuestPoses = {}
       for r, v in pairs(getnilinstances()) do
           if
               v:IsA("Model") and v:FindFirstChild("Head") and v.Head:FindFirstChild("QuestBBG") and
                   v.Head.QuestBBG.Title.Text == "QUEST" and
                   v.Name ~= "Villager"
            then
               QuestPoses[v.Name] = v:WaitForChild "HumanoidRootPart".CFrame * CFrame.new(0, -2, 2)
           end
       end
       for r, v in pairs(game.Workspace.NPCs:GetDescendants()) do
           if v.Name == "QuestBBG" and v.Title.Text == "QUEST" and v.Parent.Parent.Name ~= "Villager" then
               QuestPoses[v.Parent.Parent.Name] = v.Parent.Parent.HumanoidRootPart.CFrame * CFrame.new(0, -2, 2)
           end
       end
       DialoguesList = {}
       for r, v in pairs(require(game.ReplicatedStorage.DialoguesList)) do
           DialoguesList[v] = r
       end
       local V = getscriptclosure(game:GetService("Players").LocalPlayer.PlayerScripts.NPC)
       local W = {}
       for k, v in pairs(debug.getprotos(V)) do
           if #debug.getconstants(v) == 1 then
               table.insert(W, debug.getconstant(v, 1))
           end
       end
       local X = false
       local Y = {}
       for k, v in pairs(debug.getconstants(V)) do
           if type(v) == "string" then
               if v == "Players" then
                   X = false
               end
               if not X then
                   if v == "Blox Fruit Dealer" then
                       X = true
                   end
               else
               end
               if X then
                   table.insert(Y, v)
               end
           end
       end
       local Z = {}
       QuestPoint = {}
       for k, v in pairs(Y) do
           if QuestPoses[v] then
               Z[W[k]] = Y[k]
           end
       end
       for r, v in next, Z do
           QuestPoint[r] = QuestPoses[v]
       end
       QuestPoint["SkyExp1Quest"] =
           CFrame.new(
           -7857.28516,
           5544.34033,
           -382.321503,
           -0.422592998,
           0,
           0.906319618,
           0,
           1,
           0,
           -0.906319618,
           0,
           -0.422592998
       )
   end
   CFrameQuest()
   
   function IsMonExists(a)
       if
           a and a.Parent and a:FindFirstChild("Humanoid") and a:FindFirstChild("HumanoidRootPart") and
               a.Humanoid.Health >= 0 and
               (a.HumanoidRootPart or a.Character.HumanoidRootPart).CFrame
        then
           return true
       else
           return false
       end
   end
   
   function sQ(m, dh)
       table.foreach(
           m,
           function(g, l)
               enableaim = true
               down(l, 0)
               enableaim = false
           end
       )
   end
   function StartRaid()
       checkraid3 = true
       spawn(
           function()
               wait(10)
               checkraid3 = false
           end
       )
   
       if CheckSea(2) then
           return fireclickdetector(Workspace.Map.CircleIsland.RaidSummon2.Button.Main.ClickDetector)
       end
       fireclickdetector(Workspace.Map["Boat Castle"].RaidSummon2.Button.Main.ClickDetector)
   end
   function IsIslandRaid(cu)
       if game:GetService("Workspace")["_WorldOrigin"].Locations:FindFirstChild("Island " .. cu) then
           min = 4500
           for r, v in pairs(game:GetService("Workspace")["_WorldOrigin"].Locations:GetChildren()) do
               if
                   v.Name == "Island " .. cu and
                       (v.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude < min
                then
                   min = (v.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude
               end
           end
           for r, v in pairs(game:GetService("Workspace")["_WorldOrigin"].Locations:GetChildren()) do
               if
                   v.Name == "Island " .. cu and
                       (v.Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <= min
                then
                   return v
               end
           end
       end
   end
   function getNextIsland()
       TableIslandsRaid = {5, 4, 3, 2, 1}
       for r, v in pairs(TableIslandsRaid) do
           if
               IsIslandRaid(v) and
                   (IsIslandRaid(v).Position - game.Players.LocalPlayer.Character.HumanoidRootPart.Position).Magnitude <=
                       4500
            then
               return IsIslandRaid(v)
           end
       end
   end
   
   function CheckIsRaiding()
       local checkraid1 = getNextIsland()
       local checkraid2 = LocalPlayer.PlayerGui.Main.Timer.Visible
       return checkraid1 or checkraid2 or checkraid3
   end
   
   EnemySpawn = {}
   function SpawnPoint()
       -- if not workspace:FindFirstChild"Celestial_ENEMYSPAWM" then
       --              main_folder = Instance.new("Folder", workspace)
       --              main_folder.Name = "Celestial_ENEMYSPAWM"
       --            end
       --
       -- mobname = mobname:gsub(" .Lv. %d+.", ""):gsub(" ", "")
   
       for i, v in pairs(game.Workspace.Enemies:GetChildren()) do
           if IsMonExists(v) then
               if not EnemySpawn[v.Name] then
                   EnemySpawn[v.Name] = {} 
               end
                   table.insert(EnemySpawn[v.Name], v.HumanoidRootPart.CFrame)
           end
       end
       for i, v in pairs(game:GetService("Workspace")["_WorldOrigin"].EnemySpawns:GetChildren()) do
           if v.Name then
              if not EnemySpawn[v.Name:gsub(" .Lv. %d+.", "")] then EnemySpawn[v.Name:gsub(" .Lv. %d+.", "")] = {} end 
               table.insert(EnemySpawn[v.Name:gsub(" .Lv. %d+.", "")] , v.CFrame)
           end
       end
       for i, v in pairs(getnilinstances()) do
           if v and v.Name and not EnemySpawn[(v.Name:gsub(" .Lv. %d+.", ""))] then
              
   
               local cach = gqgqj(v)
               if cach then
                 if not EnemySpawn[v.Name:gsub(" .Lv. %d+.", "")] then EnemySpawn[v.Name:gsub(" .Lv. %d+.", "")] = {} end
                   table.insert(EnemySpawn[v.Name:gsub(" .Lv. %d+.", "")] , cach)
               end
           end
       end
   end
   function gqgqj(a)
       if a and a:FindFirstChild "HumanoidRootPart" then
           return a.HumanoidRootPart.CFrame
       end
       if a:GetAttribute("CFrame") then
           return a.CFrame
       end
   end
   SpawnPoint()
   BringDistance = {}
   
   for i, v in pairs(EnemySpawn) do
      local count = #v 
      local centerize = CFrame.new(0,0,0).p
      for i2, v2 in pairs(v) do 
         centerize = centerize + v2.p
      end 
      
      BringDistance[i] = (centerize/count).Magnitude 
      EnemySpawn[i] = CFrame.new( centerize/count)
   end 
   
   
   function Lq()
       --   if m.Buso.Value then
       Buso()
       --  end
   end
   function click2()
      game:GetService("VirtualUser"):CaptureController()
      game:GetService("VirtualUser"):Button1Down(Vector2.new(1999,999))
        if math.random(1, 2) == 2 then
           click()
        end
   end
   
   
   function down(used, hold)
       use = true
       game:service("VirtualInputManager"):SendKeyEvent(true, used, false, game)
       task.wait(hold or 0)
       game:service("VirtualInputManager"):SendKeyEvent(false, used, false, game)
       use = false
   end
 
   function equip(tooltip)
       local player = game.Players.LocalPlayer
       local character = player.Character or player.CharacterAdded:Wait()
   
       for _, item in pairs(player.Backpack:GetChildren()) do
           if item:IsA("Tool") and (item.ToolTip == tooltip or tostring(item) == tooltip) then
               local humanoid = character:FindFirstChildOfClass("Humanoid")
               if humanoid and not humanoid:IsDescendantOf(item) then
                   if not game.Players.LocalPlayer.Character:FindFirstChild(item.Name) then
                       game.Players.LocalPlayer.Character.Humanoid:EquipTool(item)
                   end
                   return item
               end
           end
       end
   end
   
   sqr = os.time()
   
   function Buso()
       if (not (game.Players.LocalPlayer.Character:FindFirstChild("HasBuso"))) then
           CommF("Buso")
       end
   end
   
   function FindAverageCFrame(cframeList)
       local totalPosition = Vector3.new(0, 0, 0)
       local validCount = 0
       for i = 1, #cframeList do
           local cframe = cframeList[i]
           totalPosition = totalPosition + cframe.Position
           validCount = validCount + 1
       end
       local averagePosition = totalPosition / validCount
       local averageCFrame = CFrame.new(averagePosition)
       return averageCFrame
   end
   
   function Bring(BringName)
       --    if not Settings["Grab Mon"].Value then
       --   return
       -- end
       BringName = tostring(BringName)
       BringList = {}
       AGuyWhoSuckedOnZooPorn = {}
       Protected_Mon = workspace.Enemies:GetChildren()
       for i, v in pairs(Protected_Mon) do
           if
               v and v.Parent and v:FindFirstChild("HumanoidRootPart") and v.Name == BringName and not v:FindFirstChild("huhu") and
                   v:FindFirstChild("HumanoidRootPart")
            then
               Instance.new("Part", v).Name = "huhu"
               table.insert(BringList, v.HumanoidRootPart.CFrame)
               table.insert(AGuyWhoSuckedOnZooPorn, v)
           end
       end
   
       BringCFrame = FindAverageCFrame(BringList)
   
       for i, v in pairs(AGuyWhoSuckedOnZooPorn) do
           if
               v and v.Name == BringName and v:FindFirstChild("HumanoidRootPart") and v:FindFirstChild("Humanoid") and
                   v.Humanoid.Health > 0 and (v.HumanoidRootPart.Position - BringCFrame.Position).Magnitude <
                       tonumber(BringDistance[BringName] / 1.5 or 0) and
                   (v.HumanoidRootPart.Position - BringCFrame.Position).Magnitude > 30
            then
               
               sethiddenproperty(game.Players.LocalPlayer, "SimulationRadius", math.huge)
   				v.HumanoidRootPart.CFrame = BringCFrame
   				v.Humanoid.JumpPower = 0
   				v.Humanoid.WalkSpeed = 0
   				
   				v.HumanoidRootPart.Transparency = 1
   				v.HumanoidRootPart.CanCollide = false
   				v.Head.CanCollide = false
   				if v.Humanoid:FindFirstChild("Animator") then
   					v.Humanoid.Animator:Destroy()
   				end
   				v.Humanoid:ChangeState(11)
   				v.Humanoid:ChangeState(14)
   				v.Humanoid.Sit = true 
   				
           end
       end
   end
   
   function getAngle()
       if not troll or os.time() - troll > 1 then
           angle = Vector3.new(math.random(-30, 30), 40, math.random(-30, 30))
           troll = os.time()
       end
       return angle
   end
   
   function getAllChildWithName(workspace, child)
       local resz = {}
       for _, b in pairs(workspace:GetChildren()) do
           if tostring(b) == tostring(child) then
               table.insert(resz, b)
           end
       end
       wait(1)
       return resz
   end
   whitelist2 = {"Factory Staff"}
   function KhangSex(im, stopfunction)
       if not stopfunction or type(stopfunction) ~= "function" then
           stopfunction = function()
               return true
           end
       end
       if type(im) == "string" then
           im = {im}
       end
       for _, mon in pairs(im) do
          
           mon = relmon[mon] or mon
           mon = checkIsNameMonNotMatch(mon)
   
           if not workspace.Enemies:FindFirstChild(mon) then
               repeat
                   task.wait()
   
                   to(EnemySpawn[mon] + Vector3.new(15,75,0))
               until CaculateDist(EnemySpawn[mon]) < 100
           end
           local cq = workspace.Enemies:FindFirstChild(mon)
           if not IsMonExists(cq) or cq.Humanoid.Health < 1 then
              
               to(EnemySpawn[mon] + Vector3.new(15,75,0))
               return
           end
           
   
          --[[ if Monsex[tostring(cq)] and not whitelist2[tostring(cq)] and #getAllChildWithName(workspace.Enemies, cq) < 3 then
           
               local cp2 = os.time()
               repeat
                   wait()
                   
                 to(EnemySpawn[mon] + Vector3.new(15,75,0))
               until #getAllChildWithName(workspace.Enemies, cq) > 2 or os.time() - cp2 > 10
           end
   ]]
         --  local cm = (cq.Humanoid.Health / cq.Humanoid.MaxHealth) * 100
           
           repeat
               task.wait()
   
                Bring(cq)
               if cq:FindFirstChild("HumanoidRootPart") then
                   Lq()
                   monp = (cq.HumanoidRootPart.CFrame)
   
                   to(monp + getAngle())
                   if not cq:FindFirstChild "Humanoid" or cq.Humanoid.Health < 1 then
                       cq:Destroy()
                   end
   
                   if false and tonumber(30) > cm and Settings["Mastery Farm"].Value then
                       equip(Settings["Select Mastery Weapon"].Value)
                       sQ(Settings["Select Mastery Skills"].Value, 0.1)
                   else
                       ---Settings["Select Weapon"].Value)
                       equip("Melee")
                   end
               end
               click2()
           until not IsMonExists(cq) or not stopfunction()
       end
   end
   
   function CheckSea(e)
       if sussea then
           if sussea == e then
               return true
           end
       else
           if game.PlaceId == 2753915549 then
               if e == 1 then
                   sussea = 1
                   return true
               end
           elseif game.PlaceId == 4442272183 then
               if e == 2 then
                   sussea = 2
                   return true
               end
           elseif game.PlaceId == 7449423635 then
               if e == 3 then
                   sussea = 3
                   return true
               end
           end
           return false
       end
   end
   
   function TeleportSea(e)
       if e == 1 then
           if not CheckSea(e) then
               local args = {[1] = "TravelMain"}
               CommF(unpack(args))
               wait()
           end
       elseif e == 2 then
           if not CheckSea(e) then
               local args = {[1] = "TravelDressrosa"}
               CommF(unpack(args))
           end
       elseif e == 3 then
           if not CheckSea(e) then
               local args = {[1] = "TravelZou"}
               CommF(unpack(args))
           end
       end
   end
   
   function getAllMobInServer()
       aO = {1475, math.huge}
       if CheckSea(2) then
           aO = {675, 1475}
       elseif CheckSea(1) then
           aO = {0, 675}
       end
   
       local m, n = {}, {}
       for a, b in pairs(Quests) do
           if a ~= "CitizenQuest" and a ~= "BartiloQuest" then
               for c, d in pairs(b) do
                   for e, f in pairs(d.Task) do
                       aG = f
                   end
   
                   if d and d.LevelReq and aO and d.LevelReq <= aO[2] and d.LevelReq >= aO[1] and aG > 1 then
                       if string.match(d.Name, "s$") then
                           d.Name = string.sub(d.Name, 1, -2)
                       end
                       table.insert(m, d.Name)
                       defaultggg = d.Name
                   end
               end
           end
       end
       return m
   end
   Monsex = {}
   for i, v in pairs(getAllMobInServer()) do
       Monsex[relmon[v] or v] = v
   end
   
   function getAllBoss()
       aO = {1475, 6000}
       if CheckSea(2) then
           aO = {675, 1475}
       elseif CheckSea(1) then
           aO = {0, 675}
       end
   
       local m, n = {}, {}
       for a, b in pairs(Q) do
           if a ~= "CitizenQuest" and a ~= "BartiloQuest" then
               for c, d in pairs(b) do
                   for e, f in pairs(d.Task) do
                       aG = f
                   end
   
                   if d and d.LevelReq and aO and d.LevelReq <= aO[2] and d.LevelReq >= aO[1] and aG == 1 then
                       table.insert(m, d.Name)
                       defaultggg2 = d.Name
                   end
               end
           end
       end
       return m
   end
   
   function getAllSpawnBoss()
       gR = {}
       for _, l in pairs(workspace.Enemies:GetChildren()) do
           if
               l:FindFirstChild("Humanoid") and l.Name ~= "Fishboat" and l.Humanoid:GetAttribute("DisplayName") and
                   (string.find(l.Humanoid.DisplayName, "Boss") or string.find(l.Humanoid.DisplayName, "Raid Boss"))
            then
               table.insert(gR, l.Name)
           end
       end
       for _, l in pairs(game.ReplicatedStorage:GetChildren()) do
           if
               l:FindFirstChild("Humanoid") and l.Humanoid:GetAttributes("DisplayName") and
                   (string.find(l.Humanoid.DisplayName, "Boss") or string.find(l.Humanoid.DisplayName, "Raid Boss"))
            then
               table.insert(gR, l.Name)
           end
       end
       return gR
   end
   
   function getMonByName(bg)
       if IsMonExists(game:GetService("ReplicatedStorage"):FindFirstChild(bg)) then
           return game:GetService("ReplicatedStorage"):FindFirstChild(bg)
       end
       if IsMonExists(game.workspace.Enemies:FindFirstChild(bg)) then
           return game.workspace.Enemies:FindFirstChild(bg)
       end
   end
   
   function checkIsNameMonNotMatch(m)
       if string.match(m, "s$") then
           return string.sub(m, 1, -2)
       end
       if m == "Swan's Pirate" then
           return "Swan Pirate"
       end
       return m
   end
   
   function GetCurrentPlayerQuest()
       local Level = PlayerData().Level
       local OldLevelReq = 0
       for NameQuest, QuestData in pairs(Quests) do
           if NameQuest ~= "BartiloQuest" and NameQuest ~= "CitizenQuest" then
               for LevelQuest, TempHuHu in pairs(QuestData) do
                   for _, QuestReq in pairs(TempHuHu.Task) do
                       QuestReq2 = QuestReq
                   end
                   if QuestReq2 ~= 1 then
                       if TempHuHu.LevelReq <= Level and TempHuHu.LevelReq >= OldLevelReq then
                           RealQuestData = {
                               NameQuest = NameQuest,
                               LevelQuest = LevelQuest,
                               LevelPassed = true,
                               NameMon = TempHuHu.Name,
                               LevelReq = TempHuHu.LevelReq
                           }
                           OldLevelReq = TempHuHu.LevelReq
                       end
                   end
               end
           end
       end
       return RealQuestData
   end
   
   function GetCustomQuest(Mob)
       local Level = PlayerData().Level
   
       for NameQuest, QuestData in pairs(Quests) do
           for LevelQuest, TempHuHu in pairs(QuestData) do
               if TempHuHu.Name == tostring(Mob) then
                   for _, QuestReq in pairs(TempHuHu.Task) do
                       QuestReq2 = QuestReq
                   end
                   if QuestReq2 ~= 1 then
                       RealQuestData = {
                           NameQuest = NameQuest,
                           LevelQuest = LevelQuest,
                           LevelPassed = TempHuHu.LevelReq <= Level,
                           NameMon = TempHuHu.Name,
                           LevelReq = TempHuHu.LevelReq
                       }
                       OldLevelReq = TempHuHu.LevelReq
                   end
               end
           end
       end
       return RealQuestData
   end
   
   function GetCompassQuest()
       local Huhu = require(game.ReplicatedStorage:WaitForChild("GuideModule"))
       for key, value in pairs(Huhu.Data) do
           if key == "QuestData" then
               return value
           end
       end
   end
   
   function CheckIsMonSameQuest(Name1, Name2)
       local matched = 1
       for NameQuest, QuestData in pairs(Quests) do
           for LevelQuest, QuestData2 in pairs(QuestData) do
               if QuestData2.Name == Name1 then
                   for LevelQuest2, temPhUhUu in pairs(QuestData) do
                       if temPhUhUu.Name == Name2 then
                           return true
                       end
                   end
               end
           end
       end
   end
   
   function CheckIsCanAllowedAllQuest(CNameQuest)
       local HuhuData = {}
       local Level = PlayerData().Level
       for NameQuest, QuestData in pairs(Quests) do
           if NameQuest == CNameQuest then
               for LevelQuest, huhuCache in pairs(QuestData) do
                   for _, huhu in pairs(huhuCache.Task) do
                       huhu2 = huhu
                   end
   
                   if huhu2 > 3 then
                       print("Level Found: ", huhuCache.Name, huhuCache.LevelReq)
                       table.insert(HuhuData, huhuCache.LevelReq)
                   end
               end
           end
       end
   
       for _, v in pairs(HuhuData) do
           if v > Level then
               return false
           end
       end
   
       return true
   end
   
   function checkMobLevel(quest, mob) 
      for NameQuest, data in pairs(Quests) do 
         if NameQuest == quest then 
            for LevelQuest, data2 in pairs(data) do 
               for i, v in pairs(data2.Task) do 
                  if i == mob or data2.Name == mob then 
                     return LevelQuest
                  end 
               end 
            end 
         end 
      end 
   end
   
   function getDoubleQuest()
       local Level = PlayerData().Level
       local CurrentQuest = GetCurrentPlayerQuest()
       print(Level, OldQuest)
       pcall(function () 
          print (OldQuest.NameMon, CheckIsMonSameQuest(OldQuest.NameMon, CurrentQuest.NameMon), CheckIsCanAllowedAllQuest(CurrentQuest.NameQuest), checkMobLevel(OldQuest.NameQuest, OldQuest.NameMon) ) 
       end) 
       
       if
           Level < 30 or not OldQuest or not CheckIsMonSameQuest(OldQuest.NameMon, CurrentQuest.NameMon) or
               not CheckIsCanAllowedAllQuest(CurrentQuest.NameQuest) or checkMobLevel(OldQuest.NameQuest, OldQuest.NameMon) == 1 
        then
           return CurrentQuest
       end
   
       for NameQuesta, QuestData in pairs(Quests) do
           if NameQuesta == CurrentQuest.NameQuest then
               for LevelQuest, huhuhu in pairs(QuestData) do
                   for _, TaskReq in pairs(huhuhu.Task) do
                       TaskCountHihi = TaskReq
                   end
   
                   if TaskCountHihi ~= 0 then
                       return {
                           Chodeststrhrj = "cmm",
                           NameQuest = NameQuesta,
                           LevelQuest = LevelQuest,
                           NameMon = huhuhu.Name,
                           LevelPassed = true,
                           LevelReq = huhuhu.LevelReq
                       }
                   end
               end
           end
       end
   end
   
   function PlayerData(Player)
       local datatable = {}
       for i, v in pairs((Player or LocalPlayer).Data:GetChildren()) do
           --    warn(v)
           pcall(
               function()
                   if v.Value then
                       datatable[v.Name] = v.Value
                   end
               end
           )
       end
       return datatable
   end
   
   function getGameTime()
       GameTime = "Error"
       local c = game.Lighting
       local ao = c.ClockTime
       if ao >= 18 or ao < 5 then
           GameTime = "Night"
       else
           GameTime = "Day"
       end
       return GameTime
   end
   
   function getMoonStatus()
       return ({
           ["http://www.roblox.com/asset/?id=9709149431"] = " [🌕] Full Moon",
           ["http://www.roblox.com/asset/?id=9709149052"] = "[🌔] 75",
           ["http://www.roblox.com/asset/?id=9709143733"] = "[🌗] 50",
           ["http://www.roblox.com/asset/?id=9709150401"] = "[🌒] 25",
           ["http://www.roblox.com/asset/?id=9709149680"] = "[🌑] 0"
       })[game:GetService("Lighting").Sky.MoonTextureId]
   end
   
   function isMirageSpawn()
       return game:GetService("Workspace").Map:FindFirstChild("MysticIsland")
   end
   
   
   function getPortal(check2)
       local check3 = check2.Position
       local w = game.PlaceId
       if w == 2753915549 then
           gQ = {
               Vector3.new(-7894.6201171875, 5545.49169921875, -380.246346191406),
               Vector3.new(-4607.82275390625, 872.5422973632812, -1667.556884765625),
               Vector3.new(61163.8515625, 11.759522438049316, 1819.7841796875),
               Vector3.new(3876.280517578125, 35.10614013671875, -1939.3201904296875)
           }
       elseif w == 4442272183 then
           gQ = {
               Vector3.new(-288.46246337890625, 306.130615234375, 597.9988403320312),
               Vector3.new(2284.912109375, 15.152046203613281, 905.48291015625),
               Vector3.new(923.21252441406, 126.9760055542, 32852.83203125),
               Vector3.new(-6508.5581054688, 89.034996032715, -132.83953857422)
           }
       elseif w == 7449423635 then
           gQ = {
               Vector3.new(-5058.77490234375, 314.5155029296875, -3155.88330078125),
               Vector3.new(5756.83740234375, 610.4240112304688, -253.9253692626953),
               Vector3.new(-12463.8740234375, 374.9144592285156, -7523.77392578125),
               Vector3.new(-11993.580078125, 334.7812805175781, -8844.1826171875),
               Vector3.new(5314.58203125, 25.419387817382812, -125.94227600097656)
           }
       end
       local aM, aN = Vector3.new(0, 0, 0), math.huge
   
       for _, aL in pairs(gQ) do
           if (aL - check3).Magnitude < aN and aM ~= aL then
               aM, aN = aL, (aL - check3).Magnitude
           end
       end
       return aM
   end
   
   function grgrgrgrg(is)
       wait(.5)
       repeat
           task.wait()
           game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid"):ChangeState(15)
           LocalPlayer.Character.HumanoidRootPart.CFrame = is
       until LocalPlayer.Character.PrimaryPart.CFrame == is
       game:GetService("Players").LocalPlayer.Character:WaitForChild("Humanoid", 9):ChangeState(15)
       LocalPlayer.Character:SetPrimaryPartCFrame(is)
       wait(0.1)
       LocalPlayer.Character.Head:Destroy()
       repeat
           task.wait()
       until LocalPlayer.Character:FindFirstChild("Humanoid").Health <= 0
       repeat
           task.wait()
       until LocalPlayer.Character:FindFirstChild("Head")
       wait(0.5)
   end
   
   NpcList = {}
   
   for i, v in pairs(game.Workspace.NPCs:GetChildren()) do
       if string.find(string.lower(v.Name), "home point") then
           table.insert(NpcList, v:GetModelCFrame())
       end
   end
   for i, v in pairs(getnilinstances()) do
       if string.find(string.lower(v.Name), "home point") then
           table.insert(NpcList, v:GetModelCFrame())
       end
   end
   
   function m0(q)
       if q then
           return LocalPlayer.PlayerGui.Main.InCombat.Visible
       end
       return LocalPlayer.PlayerGui.Main.InCombat.Visible and LocalPlayer.PlayerGui.Main.InCombat.Text and
           (string.find(string.lower(LocalPlayer.PlayerGui.Main.InCombat.Text), "risk"))
   end
   
   function getSpawn(pos)
       pos = Vector3.new(pos.X, pos.Y, pos.Z)
       local lll, mmm = nil, math.huge
       for i, v in pairs(NpcList) do
           if (v.p - pos).Magnitude < mmm then
               lll = v
               mmm = (v.p - pos).Magnitude
           end
       end
       return lll
   end
   
   function request(check1)
       CommF(unpack({"requestEntrance", check1}))
       wait()
   end
   
   function CaculateDist(I, II)
       if not II then
           II = game.Players.LocalPlayer.Character.PrimaryPart.CFrame
       end
   
       return (Vector3.new(I.X, 0, I.Z) - Vector3.new(II.X, 0, II.Z)).Magnitude
   end
   
   function to(vg1)
       AwaitPlayer(LocalPlayer)
       if LocalPlayer.Character.Humanoid.Health < 1 then
           wait(3)
       end
   
       if tweenses then
           tweenses:Cancel()
       end
       if Humanoid and Humanoid.Sit then
           Humanoid:ChangeState(Enum.HumanoidStateType.Jumping)
       end
   
       local pos = CFrame.new(vg1.x, vg1.y, vg1.z)
       local portal = getPortal(pos)
       local req = CheckIsRaiding()
       if not req and CaculateDist(portal, pos) < CaculateDist(pos) and CaculateDist(portal) > 500 then
           return request(portal)
       end
       local bypasspos = getSpawn(pos)
       if
           not CheckSea(1) and not findItem("Fruit") and CaculateDist(pos) - CaculateDist(bypasspos, pos) > 1000 and CaculateDist(bypasspos) > 2500 and
               not m0() and
               not req
        then
           return grgrgrgrg(bypasspos)
       end
   
       for a, b in pairs(game.Players.LocalPlayer.Character:GetDescendants()) do
           if b:IsA "BasePart" then
               b.CanCollide = false
           end
       end
       if not game.Players.LocalPlayer.Character:WaitForChild "Head":FindFirstChild "eltrul" then
           local ngu = Instance.new("BodyVelocity", game.Players.LocalPlayer.Character.Head)
           ngu.Name = "eltrul"
           ngu.MaxForce = Vector3.new(0, math.huge, 0)
           ngu.Velocity = Vector3.new(0, 0, 0)
       end
       if CaculateDist(pos) < 100 then
           wait()
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = pos
           return
       end
   
       local plpos = game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame
       game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(plpos.X, pos.Y + 200, plpos.Z)
       tweenses =
           game:GetService("TweenService"):Create(
           game.Players.LocalPlayer.Character.HumanoidRootPart,
           TweenInfo.new(CaculateDist(pos) / 340, Enum.EasingStyle.Linear),
           {CFrame = CFrame.new(pos.X, pos.Y + 200, pos.Z)}
       )
       tweenses:Play()
   end
   
   local bM = {}
   
   function HopServer(bO)
       -- noti("Hop", "9999")
   
       if not bO then
           bO = 10
       end
       ticklon = tick()
       repeat
           task.wait()
       until tick() - ticklon >= 2
       local function Hop()
           --   noti("Hop Server", "Joining...")
           for r = 1, 100 do
               --  ChooseRegion = a0.HopRegion.Value
               if ChooseRegion == nil or ChooseRegion == "" then
                   ChooseRegion = "Singapore"
               else
                   game:GetService("Players").LocalPlayer.PlayerGui.ServerBrowser.Frame.Filters.SearchRegion.TextBox.Text =
                       ChooseRegion
               end
               local bP = game:GetService("ReplicatedStorage").__ServerBrowser:InvokeServer(r)
               for k, v in pairs(bP) do
                   if k ~= game.JobId and v["Count"] < bO then
                       if not bM[k] or tick() - bM[k].Time > 60 * 10 then
                           bM[k] = {Time = tick()}
   
                           if game:GetService("Players").LocalPlayer.PlayerGui.Main.InCombat.Visible then
                               --   noti("Wait For End InCombat", 15)
                               repeat
                                   wait()
                                   to(CFrame.new(0, math.random(9999, 99999), 0))
                               until not check11()
                           else
                           end
                           game:GetService("ReplicatedStorage").__ServerBrowser:InvokeServer("teleport", k)
                           return true
                       elseif tick() - bM[k].Time > 60 * 60 then
                           bM[k] = nil
                       end
                   end
               end
           end
           return false
       end
       if not getgenv().Loaded then
           local function bQ(v)
               if v.Name == "ErrorPrompt" then
                   if v.Visible then
                       if v.TitleFrame.ErrorTitle.Text == "Teleport Failed" then
                           HopServer()
                           v.Visible = false
                       end
                   end
                   v:GetPropertyChangedSignal("Visible"):Connect(
                       function()
                           if v.Visible then
                               if v.TitleFrame.ErrorTitle.Text == "Teleport Failed" then
                                   HopServer()
                                   v.Visible = false
                               end
                           end
                       end
                   )
               end
           end
           for k, v in pairs(game.CoreGui.RobloxPromptGui.promptOverlay:GetChildren()) do
               bQ(v)
           end
           game.CoreGui.RobloxPromptGui.promptOverlay.ChildAdded:Connect(bQ)
           getgenv().Loaded = true
       end
       while not Hop() do
           wait()
       end
       SaveSettings2()
   end
   
   function checkInventory(inp)
       if type(inp) == "string" then
           inp = {inp}
       end
       for u, v in pairs(inp) do
           if LocalPlayer.Backpack:FindFirstChild(v) or LocalPlayer.Character:FindFirstChild(v) then
               return v
           end
       end
   end
   
   function getPlayerHunterQuest()
       if PlayerData().Level < 60 or checkIsUnlockedSea(2) then
           return
       end
       local playerHunterQuestData = CommF("PlayerHunter")
       if not string.find(tostring(playerHunterQuestData), "later") then
           local eltime = os.time()
           while wait() do
               CommF("PlayerHunter")
               local enemy = Players[QuestLib:getNameQuest()]
               if enemy then
                   local data = PlayerData(enemy)
                   local enemy = AwaitPlayer(enemy)
                   local plrdata = PlayerData()
                   local huhu = AwaitPlayer(LocalPlayer)
   
                   if enemy.Character.Humanoid.Health < (huhu.Character.Humanoid.Health + 350) then
                       if data.Level > 20 then
                           return enemy
                       end
                   end
   
                   QuestLib:Abandon()
               end
               if os.time() - eltime > 10 then
                   HopServer()
                   repeat
                       wait()
                   until false
               end
           end
       end
   end
   
   function AwaitPlayer(Plr)
       local huhu =
           Plr and Plr.Character and Plr.Character:WaitForChild("Humanoid") and
           Plr.Character:WaitForChild("HumanoidRootPart") and
           Plr.Character:WaitForChild("Head")
       return Plr
   end
   AwaitPlayer(LocalPlayer).PlayerGui.Notifications.ChildAdded:Connect(
       function(v)
           v.Visible = false
       end
   )
   Notifi = {}
   local env = getgenv()
   local dotehookold
   dotehookold =
       hookfunction(
       require(game:GetService("ReplicatedStorage").Notification).new,
       function(...)
           Args = {...}
           spawn(
               function()
                   keys = #Notifi
                   wait(10)
                   table.remove(Notifi, keys)
               end
           )
           return dotehookold(...)
       end
   )
   function rollFruit()
       if alrroll then
           return
       end
       game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("Cousin", "Buy")
       alrroll = true
   end
   
   function CheckNotify(msg)
       for i, v in pairs(Notifi) do
           if string.find(string.lower(tostring(v)), string.lower(msg)) then
               table.remove(Notifi, v)
               return true
           end
       end
   end
   
   function RedeemAllCodesX2()
       CodesX2 = {
           "Sub2CaptainMaui",
           "CODE_SERVICIO",
           "CINCODEMAYO_BOOST",
           "15B_BESTBROTHERS",
           "DEVSCOOKING",
           "GAMERROBOT_YT",
           "ADMINGIVEAWAY",
           "GAMER_ROBOT_1M",
           "TY_FOR_WATCHING",
           "kittgaming",
           "Sub2Fer999",
           "Enyu_is_Pro",
           "Magicbus",
           "JCWK",
           "Starcodeheo",
           "Bluxxy",
           "fudd10_v2",
           "FUDD10",
           "BIGNEWS",
           "THEGREATACE",
           "SUB2GAMERROBOT_EXP1",
           "Sub2OfficialNoobie",
           "StrawHatMaine",
           "SUB2NOOBMASTER123",
           "Sub2Daigrock",
           "Axiore",
           "TantaiGaming"
       }
       for r, v in pairs(CodesX2) do
           game:GetService("ReplicatedStorage").Remotes.Redeem:InvokeServer(v)
       end
   end
   
   function n4(g1)
       if not og1 then
           return Vector3.new(0, 0, 0)
       end
       return (g1 - og1)
   end
   
   function move_predict(plr)
       if not plr then
           return Vector3.new(0, 0, 0)
       end
       ai1 = n4(plr.Character.Head.Position)
       og1 = plr.Character.Head.Position
       return ai1
   end
   
   function getToolFromTooltip(tooltip)
       for _, tool in pairs(LocalPlayer.Character:GetChildren()) do
           if tool:IsA("Tool") and tool.ToolTip == tooltip then
               return tool
           end
       end
       for _, tool in pairs(LocalPlayer.Backpack:GetChildren()) do
           --   if tool:IsA("Tool") then
   
           if tool:IsA("Tool") and tool.ToolTip == tooltip then
               return tool
           end
   
           --   end
       end
   end
   
   function GetUseableSkills(Tool)
       Tool = Tool or "Melee"
   
       toolname = getToolFromTooltip(Tool)
       if toolname then
           local toolname = tostring(toolname)
           local toolgui = LocalPlayer.PlayerGui.Main.Skills:FindFirstChild(toolname)
           for ind, var in pairs({"Z", "X", "V", "C", "F"}) do
               if toolgui:FindFirstChild(var) then
                   if toolgui[var].Cooldown.AbsoluteSize.X < 2 then
                       return var
                   end
               end
           end
       end
   end
   
   function nung()
       if game:GetService("Players").LocalPlayer.PlayerGui.Main.PvpDisabled.Visible == true then
           CommF("EnablePvp")
       end
   end
   
   function getPlayerInventory()
       return CommF("getInventory")
   end
   
   function findItem(item)
       for i, v in pairs(LocalPlayer.Backpack:GetChildren()) do
           if string.find(string.lower(tostring(v)), string.lower(item)) then
               return v
           end
       end
       for i, v in pairs(LocalPlayer.Character:GetChildren()) do
           if v:IsA("Tool") and string.find(string.lower(tostring(v)), string.lower(item)) then
               return v
           end
       end
   end
   
   function CheckItem(sw)
       local inv2 = LocalPlayer.Backpack:FindFirstChild(sw) or LocalPlayer.Character:FindFirstChild(sw)
   
       for k, v in pairs(getPlayerInventory()) do
           if v.Name == sw then
               return v
           end
       end
       return inv2
   end
   
   function MeleeHandling()
       if not rua then
           return
       end
       local level = PlayerData().Level
       local fragments = PlayerData().Fragments
       local beli = PlayerData().Beli
       if beli > 750000 and rua["Electro"] >= 350 and rua["FishmanKarate"] == -1 then
          print("Purchase FishmanKarate")
          BuyMelee("FishmanKarate")
       elseif beli > 500000 and rua["BlackLeg"] >= 350 and rua["Electro"] == -1 then
          print("Purchase Electro")
           BuyMelee("Electro")
       elseif beli > 150000 and rua["BlackLeg"] == -1 then
           BuyMelee("BlackLeg")
          print("Purchase BlackLeg")
       end
   end
   
   function getSaberQuestProcess()
       if PlayerData().Level < 200 then
           return false
       end
       if temporary.SaberQuest then
           return false
       end
       if CheckItem("Saber") then
           temporary.SaberQuest = true
           return -1
       end
   
       local map = game:GetService("Workspace").Map.Jungle
       local cmm = CommF("ProQuestProgress", "RichSon")
   
       if map.QuestPlates.Door.CanCollide then
           return 1 --1: first quest
       elseif game:GetService("Workspace").Map.Desert.Burn.Part.CanCollide then
           return 2 -- 2: sec quest
       elseif not cmm then
           return 3
       elseif cmm == 0 then
           if not CheckItem("Relic") then
               return 4
           end
       elseif map.Final.Part.CanCollide then
           return 5
       elseif getMonByName("Saber Expert") then
           return 6
       end
   end
   
   function unlocksea2()
       if not CheckItem "Saber" then
           HopServer()
           wait(99)
       end
       if not CheckItem("Key") then
           CommF("DressrosaQuestProgress", "Detective")
       end
       if game.Workspace.Map.Ice.Door.CanCollide then
           equip("Key")
           to(game.Workspace.Map.Ice.Door.CFrame)
       else
           KhangSex("Ice Admiral")
       end
       TeleportSea(2)
   end
   
   function CheckCanBuyMelee(melee)
       if CommF("Buy" .. melee, true) == 1 or CommF("BlackbeardReward", melee, "1") == 1 then
           return true
       end
   end
   function BuyMelee(melee)
       return CommF("Buy" .. melee)
   end
   
   function getWeaponByTooltip(ttip)
       for i, v in pairs({unpack(LocalPlayer.Backpack:GetChildren()), unpack(LocalPlayer.Character:GetChildren())}) do
           if v and v:IsA("Tool") and v.ToolTip and v.ToolTip == ttip then
               return v
           end
       end
   end
   temporary.sea = {}
   function checkIsUnlockedSea(sea)
       if temporary.sea[sea] == true then
           return true
       end
   
       local id = ({2125253106, 2125253113})[sea - 1]
   
       local res = game:GetService("BadgeService"):UserHasBadge(LocalPlayer.UserId, id)
   
       if res == true then
           temporary.sea[sea] = true
       end
       return res
   end
   
   function smartStore() 
      
       for i,v in pairs(game.Players.LocalPlayer.Backpack:GetChildren()) do
      
           if v:IsA("Tool") and string.find(v.Name, "Fruit") then
               game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("StoreFruit",OriginalName2[v.Name],v)
              --  sendwh(Config.Webhook, "Stored "..tostring(v))
           end
       end
       for i,v in pairs(game.Players.LocalPlayer.Character:GetChildren()) do
           if v:IsA("Tool") and string.find(v.Name, "Fruit") then
               game:GetService("ReplicatedStorage").Remotes.CommF_:InvokeServer("StoreFruit",OriginalName2[v.Name],v)
            --   sendwh(Config.Webhook, "Stored "..tostring(v))
           end
       end 
    --  Humanoid.Health = 0 
    -- wait(2)
   end
   
   function Bartilo2()
       if
           (CFrame.new(
               -1837.46155,
               44.2921753,
               1656.1987,
               0.999881566,
               -1.03885048e-22,
               -0.0153914848,
               1.07805858e-22,
               1,
               2.53909284e-22,
               0.0153914848,
               -2.55538502e-22,
               0.999881566
           ).Position - game.Players.LocalPlayer.Character.Head.Position).magnitude > 500
        then
           to(
               CFrame.new(
                   -1837.46155,
                   44.2921753,
                   1656.1987,
                   0.999881566,
                   -1.03885048e-22,
                   -0.0153914848,
                   1.07805858e-22,
                   1,
                   2.53909284e-22,
                   0.0153914848,
                   -2.55538502e-22,
                   0.999881566
               )
           )
       else
           game:GetService("Players").LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1836, 11, 1714)
           wait(.5)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1850.49329, 13.1789551, 1750.89685)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1858.87305, 19.3777466, 1712.01807)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1803.94324, 16.5789185, 1750.89685)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1858.55835, 16.8604317, 1724.79541)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1869.54224, 15.987854, 1681.00659)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1800.0979, 16.4978027, 1684.52368)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1819.26343, 14.795166, 1717.90625)
           wait(1)
           game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-1813.51843, 14.8604736, 1724.79541)
       end
   end
   
   function spin() 
      
       if not game.Players.LocalPlayer.Character:FindFirstChild"Spin" then 
                             local spin = Instance.new("BodyAngularVelocity")
    spin.Name = "Spin"
    spin.Parent = HumanoidRootPart
    spin.MaxTorque = Vector3.new(0, math.huge, 0)
    spin.AngularVelocity = Vector3.new(0, 40, 0)
       end
                             spawn(function () 
                                wait(10) 
                                LocalPlayer.Character.Spin:Destroy()
                             end)
   end 
                          
   
   
   raidlist = {}
   for i, v in pairs(require(game:GetService("ReplicatedStorage").Raids).advancedRaids) do
       table.insert(raidlist, v)
   end
   for i, v in pairs(require(game:GetService("ReplicatedStorage").Raids).raids) do
       table.insert(raidlist, v)
   end
   
   function getraidchip()
       for i, v in pairs(raidlist) do
           if string.find(PlayerData().DevilFruit, v) then
               return v
           end
       end
       return Config["Misc - Default Raid Type"]
   end
   function CheckFruitRare(fruit)
   end
   
   function GetBartiloQuestProcess()
       return CommF("BartiloQuestProgress", "Bartilo")
   end
   
   function CheckAllMelee()
       for _, melee in pairs(Melees) do
           if CheckCanBuyMelee(melee) then
               BuyMelee(melee)
               wait(.2)
               AwaitPlayer(LocalPlayer)
               local meleemastery = getWeaponByTooltip("Melee"):GetChildren("Level").Value
               rua[melee] = meleemastery
               print(melee, meleemastery)
           else
               rua[melee] = -1
               print(melee, false)
           end
       end
   end
   
   
   function GetPointAddData()
       local maxhealth = AwaitPlayer(LocalPlayer).Character.Humanoid.MaxHealth
       local level = PlayerData().Level
       if level > 2100 then 
          return "Gun"
       elseif level > 1800 then 
          return "Blox Fruit"
       elseif level > 1500 then
          return "Sword"
       elseif level > 1200 and maxhealth < 5000 then
           return "Defense"
       elseif level > 800 and maxhealth < 3000 then
           return "Defense"
       elseif level > 400 and maxhealth < 1000 then
           return "Defense"
       elseif level > 150 and maxhealth < 350 then
           return "Defense"
       else
           return "Melee"
       end
   end
   
   function CheckBartilo1()
       return getMonByName("Jeremy")
   end
   
   function GetSpawnFruit()
       for i, v in pairs(workspace:GetChildren()) do
           if string.find(string.lower(tostring(v)), "fruit") and not Players:FindFirstChild(v.Name) then
               return v
           end
       end
   end
   local level = PlayerData().Level
   print(level)
   -- $task
   
   TaskHandler:AddTask(
       "Default Farm",
       function()
           -- TaskHandler:CancelAll()
           print("deffarm")
           
           local quest = getDoubleQuest()
           local level = PlayerData().Level
           if level >= 700 and not checkIsUnlockedSea(2) then
               return unlocksea2()
           end
           if quest then
               local enemy = getPlayerHunterQuest()
               if level > 59 and CheckSea(1) and enemy then
                   local ElapsedTime = os.time()
                   repeat
                       task.wait()
                       equip("Melee")
                       nung()
                       Lq()
   
                       to(AwaitPlayer(enemy).Character.HumanoidRootPart.CFrame + move_predict(AwaitPlayer(enemy)))
                       if CaculateDist(AwaitPlayer(enemy).Character.HumanoidRootPart.CFrame) < 35 then
                           local move = GetUseableSkills()
                           if move then
                               to(AwaitPlayer(enemy).Character.HumanoidRootPart.CFrame + move_predict(AwaitPlayer(enemy)))
                               --                   await(LocalPlayer).Character.HumanoidRootPart.Anchored = true
                               down(move)
                               wait()
                           --  await(LocalPlayer).Character.HumanoidRootPart.Anchored = false
                           end
                       end
                   until not enemy or AwaitPlayer(enemy).Character.Humanoid.Health < 1 or os.time() - ElapsedTime > 45 or
                       CheckNotify("player")
               elseif level > 200 and SpawnBoss then 
               
                       KhangSex(tostring(SpawnBoss))
                   
               else 
                   KhangSex("Shanda")
               end
                   await(StartQuest, quest)
                   while QuestLib:isClaimed() and wait() do
                       KhangSex(quest.NameMon)
                   end
               end
           
       end,
       function()
           return TaskHandler:PrimaryTask("Default Farm")
       end,
       1
   ):Prepare()
   
   TaskHandler:AddTask(
       "SaberTask",
       function()
           TaskHandler:CancelAll("SaberTask")
           local saberProc = getSaberQuestProcess()
           wait(1)
   
           if saberProc == 1 then
               for r, v in next, game:GetService("Workspace").Map.Jungle.QuestPlates:GetChildren() do
                   if v:IsA("Model") then
                       if v.Button:FindFirstChild("TouchInterest") then
                           repeat
                               wait()
                               to(v.Button.CFrame)
                           until AwaitPlayer(LocalPlayer):DistanceFromCharacter(v.Button.Position) < 5
                       end
                   end
               end
           elseif saberProc == 2 then
               if not CheckItem "Torch" then
                   to(game:GetService("Workspace").Map.Jungle.Torch.CFrame)
                   wait(1)
               else
                   to(
                       CFrame.new(
                           1115.40015,
                           6.36537027,
                           4349.0791,
                           -0.70068568,
                           0.0279242508,
                           0.712923467,
                           1.8674281e-07,
                           0.999233782,
                           -0.0391384773,
                           -0.713470101,
                           -0.0274236482,
                           -0.700148821
                       )
                   )
                   equip("Torch")
                 
                   Humanoid:MoveTo(workspace.Map.Desert.Burn.Part.Position)
               end
           elseif saberProc == 3 then
               CommF("ProQuestProgress", "GetCup")
               CommF("ProQuestProgress", "GetCup")
               equip("Cup")
               CommF("ProQuestProgress", "FillCup", game:GetService("Players").LocalPlayer.Character.Cup)
               CommF("ProQuestProgress", "SickMan")
           elseif saberProc == 4 then
               KhangSex("Mob Leader")
           elseif saberProc == 5 then
               to(
                   CFrame.new(
                       -1405.0343,
                       31.2640743,
                       3.88229084,
                       0.899864614,
                       -0.0190337393,
                       0.435753644,
                       -0.0192312859,
                       0.996344447,
                       0.0832344145,
                       -0.435744971,
                       -0.0832798034,
                       0.896209061
                   )
               )
               equip("Relic")
               Humanoid:MoveTo(
                   CFrame.new(
                       -1405.0343,
                       31.2640743,
                       3.88229084,
                       0.899864614,
                       -0.0190337393,
                       0.435753644,
                       -0.0192312859,
                       0.996344447,
                       0.0832344145,
                       -0.435744971,
                       -0.0832798034,
                       0.896209061
                   ).p)
             --  lockcam(game:GetService("Workspace").Map.Jungle.Final.Part.Position)
               
           elseif saberProc == 6 then
               KhangSex("Saber Expert")
           end
       end,
       function()
           return getSaberQuestProcess()
       end,
       3
   ):Prepare()
   
   TaskHandler:AddTask(
       "Raid",
       function()
           TaskHandler:CancelAll("Raid")
           local RaidIsland = getNextIsland()
           if RaidIsland then
               to(RaidIsland.CFrame + Vector3.new(math.random(-100, 100), 100, math.random(-100, 100)))
               for nigg, a in pairs(workspace.Enemies:GetChildren()) do
                   if CaculateDist(a.HumanoidRootPart.CFrame) < 5000 then
                       a.HumanoidRootPart.CanCollide = false
                       sethiddenproperty(game:GetService("Players").LocalPlayer, "SimulationRadius", math.huge)
                       a.Humanoid.Health = 0
                       a:BreakJoints()
                   end
               end
           end
           CommF("Awakener", "Awaken")
       end,
       function()
           return (findItem("chip") or CheckIsRaiding())
       end,
       3
   )
   
   TaskHandler:AddTask(
       "FruitSpawn",
       function()
           local fr = GetSpawnFruit()
           repeat
               wait()
               TaskHandler:CancelAll("FruitSpawn")
               to(fr.Handle.CFrame)
           until fr.Parent ~= workspace
           print("collected")
       end,
       function()
           return GetSpawnFruit()
       end,
       6
   ):Prepare()
   
   TaskHandler:AddTask(
       "Bartilo",
       function()
          TaskHandler:CancelAll("Bartilo")
           local BartiloProcess = GetBartiloQuestProcess()
           if BartiloProcess == 0 then
               if QuestLib:isClaimed() then
                   if not string.find(QuestLib:getNameQuest(), "Swan") then
                      print("Abandon")
                       QuestLib:Abandon()
                   end
               else
                   QuestLib:StartQuest({NameQuest = "BartiloQuest", LevelQuest = 1})
               end
               KhangSex("Swan Pirate")
           elseif BartiloProcess == 1 then
               KhangSex("Jeremy")
           elseif BartiloProcess == 2 then
               Bartilo2()
           end
           wait(1)
       end,
       function()
           local BartiloProcess = GetBartiloQuestProcess()
           return CheckSea(2) and PlayerData().Level > 850 and BartiloProcess ~= 3 and
               (not BartiloProcess == 2 or CheckBartilo1())
       end,
       2
   ):Prepare()
   
   TaskHandler:AddTask(
       "background",
       function()
           while wait(5) do
          
               if CheckNotify("can only store") then
                   local Fruit = findItem("Fruit")
                   if
                       CheckFruitRare(PlayerData().DevilFruit) < CheckFruitRare(OriginalName[tostring(Fruit)]) and
                           not table.find(Config["Misc - Lock Devil Fruit"], tostring(PlayerData().DevilFruit.Value))
                    then
                       equip(Fruit.Name)
                       wait(1)
                       Fruit.EatRemote:InvokeServer()
                   elseif PlayerData().Level > 1200 then
                       CommF("RaidsNpc", "Select", GetCurrentFruitChip())
                   else
                       Fruit:Destroy()
                   end
               end
               rollFruit()
               smartStore()
               CommF("AddPoint", GetPointAddData(), 100)
             --  Panel.Text =
              --[[     "Level: " ..
                   PlayerData().Level ..
                       " | Elapsed: " ..
                           os.time() - Start_Time ..
                               " | Beli: " .. PlayerData().Beli .. " | Fragments: " .. PlayerData().Fragments
   ]]
               MeleeHandling()
               meleesuwu = ""
   
               for i, v in pairs(rua) do
                   if v ~= -1 then
                       meleesuwu = meleesuwu .. " " .. i .. "(" .. v .. ")"
                   end
               end
   
              -- Panel2.Text = "Melee(s): " .. meleesuwu
               local m1s = getWeaponByTooltip("Melee")
               if not m1s or not MeleeConverter[tostring(m1s)] then
               else
                   rua[MeleeConverter[m1s.Name]] = m1s:WaitForChild("Level").Value
               end
           end
       end,
       function()
           return true
       end,
       0
   ):Start()
   
   TaskHandler:AddTask(
       "Factory",
       function()
           TaskHandler:CancelAll("Factory")
           KhangSex("Core")
       end,
       function()
           return getMonByName("Core")
       end,
       9
   ):Prepare()
   
   RedeemAllCodesX2()
   print("Saber", getSaberQuestProcess())
   
               game:GetService("CoreGui").RobloxPromptGui.promptOverlay.ChildAdded:Connect(function(child)
               if child.Name == 'ErrorPrompt' and child:FindFirstChild('MessageArea') and child.MessageArea:FindFirstChild("ErrorFrame") then
                 
                  game:GetService("TeleportService"):Teleport(game.PlaceId)
               end
               end)
              