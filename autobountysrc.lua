    local check = loadstring(game:HttpGet("https://raw.githubusercontent.com/Dextral-Code/lua/refs/heads/main/canuseautobountyv2.lua"))()
    if check.bountyhavekey.canuse then
        spawn(function()

            if scriptloaded then
                warn "Grape Haven Auto Bounty is already loaded"
                --return 
            end

            fl = function(v)
                table.foreach(v[1], v[2])
                return v
            end

            func = function(v)
                v[1][v[2]] = v[3]
                getgenv()[v[2]] = v[3]
            end

            func {
                getgenv(),
                "set",
                function(v)
                    if type(v[1]) == "table" then
                        for _, s in pairs(v) do
                            getgenv()[s[1]] = s[2]
                        end
                    else
                        getgenv()[v[1]] = v[2]
                    end
                end
            }

            set {
                "bounty",
                {
                    ["targ"] = nil,
                    ["old killed"] = nil,
                    ["targ busy"] = nil,
                    ["checked"] = {},
                    ["hop"] = false,
                    ["can use weapon"] = false,
                    ["using skill"] = false,
                    ["weapon using"] = "",
                    ["tool tip using"] = "Melee",
                    ["cframe target"] = CFrame.new(Vector3.new(0,0,0)),
                    ["digit"] = CFrame.new(Vector3.new(0,0,0)),
                    ["tween cframe"] = CFrame.new(Vector3.new(0,0,0)),
                    ["tween set cframe"] = true,
                    ["functions"] = {
                        ["main"] = {},
                        ["misc"] = {}
                    }
                }
            }

            set {
                {"plrs", game:GetService "Players"},
                {"rs", game:GetService "ReplicatedStorage"},
                {"rstep", game:GetService("RunService").RenderStepped},
                {"workspace", game:GetService "Workspace"},
                {"vim", game:GetService "VirtualInputManager"},
                {"coregui", game:GetService "CoreGui"}
            }

            set {"lp", plrs.LocalPlayer}
            set {"comf", rs.Remotes.CommF_}

            if config.Misc["Hide Infomation"] then
                coregui.PlayerList.Enabled = false
                fl {
                    {"Chat"},
                    function(i, v)
                        lp.PlayerGui[v].Enabled = false
                    end
                }
            end
            
            func {
                bounty.functions.misc,
                "int",
                function(v)
                    return tonumber(v)
                end
            }

            func {
                bounty.functions.misc,
                "str",
                function(v)
                    return tostring(v)
                end
            }

            func {
                bounty.functions.misc,
                "wt",
                function(z)
                    if not z then z = 0 end
                    return task.wait(int(z))
                end
            }

            func {
                bounty.functions.misc,
                "ffc",
                function(i, v)
                    return i and v and i:FindFirstChild(str(v))
                end
            }

            func {
                bounty.functions.misc,
                "ffcoc",
                function(i, v)
                    return i and v and i:FindFirstChildOfClass(str(v))
                end
            }

            func {
                bounty.functions.misc,
                "wfc",
                function(i, v)
                    return i and v and i:WaitForChild(str(v))
                end
            }

            func {
                bounty.functions.misc,
                "wfhrp",
                function(v)
                    return wfc(v.Character, "HumanoidRootPart")
                end
            }

            func {
                bounty.functions.misc,
                "wfh",
                function(v)
                    return wfc(v.Character, "Humanoid")
                end
            }

            func {
                bounty.functions.misc,
                "hrp",
                function(v)
                    return wfc(v.Character, "HumanoidRootPart")
                end
            }

            func {
                bounty.functions.misc,
                "inv",
                function(i, v)
                    if not i or not v then return end
                    return i:InvokeServer(unpack(v))
                end
            }

            spawn(function()
                set {"sitinklib", loadstring(game:HttpGet("https://github.com/ErutTheTeru/uilibrary/blob/main/Sitink%20Lib/Source.lua?raw=true"))()}
            end)

            func {
                bounty.functions.misc,
                "noti",
                function(v)
                    if sitinklib then
                        sitinklib:Notify({
                            ["Title"] = "Grape Haven",
                            ["Description"] = "Auto Bounty [ Beta ]",
                            ["Content"] = v:gsub("<[^>]+>", "") or "",
                            ["Time"] = 0.5,
                            ["Delay"] = 5
                        })
                    else
                        print(v or "")
                    end
                end
            }

            lp.PlayerGui.Notifications.DescendantAdded:Connect(function(v)
                if v.Name == "TranslateMe" and string.find(string.lower(v.Text), string.lower(lp.Name)) then
                    v.Text = "Detect Infomation Local Player, Retexted it"
                end
            end)

            noti("Key Auth")
            func {
                bounty.functions.misc,
                "join",
                function(v)
                    if not (v == "Pirates" or v == "Marines") then v = "Pirates" end
                    inv(comf, {"SetTeam", v})
                    if ffc(lp.PlayerGui.Main, "ChooseTeam") then
                        ffc(lp.PlayerGui.Main, "ChooseTeam"):Destroy()
                        for i, v in pairs({"RaceEnergy","Compass","Energy","AlliesButton","Code","CrewButton","HomeButton",
                        "Mute","Settings","MenuButton","Beli","Fragments","Level",--"Radar",
                        "HP","Chat"}) do
                            if v == "RaceEnergy" then
                                if ffc(lp.Backpack, "Awakening") or ffc(lp.Character, "Awakening") then
                                    lp.PlayerGui.Main[v].Visible = true
                                end
                            else
                                if ffc(lp.PlayerGui.Main, v) then
                                    lp.PlayerGui.Main[v].Visible = true
                                end
                            end
                        end
                        workspace.CurrentCamera.CameraType = Enum.CameraType.Custom
                        workspace.CurrentCamera.CameraSubject = wfh(lp)
                        workspace.CurrentCamera.CFrame = wfhrp(lp).CFrame
                    end
                    return v
                end
            }

            func {
                bounty.functions.misc,
                "cacb",
                function()
                    fl { 
                        lp.Character:GetDescendants(), 
                        function(i, v)
                            if v:IsA("LocalScript") then
                                if v.Name == "General" or v.Name == "Shiftlock" or v.Name == "FallDamage" or v.Name == "4444" or v.Name == "CamBob" or v.Name == "JumpCD" or v.Name == "Looking" or v.Name == "Run" then
                                    v:Destroy()
                                end
                            end
                        end
                    }

                    fl {
                        lp.PlayerScripts:GetDescendants(), 
                        function(i, v)
                            if v:IsA("LocalScript") then
                                if v.Name == "RobloxMotor6DBugFix" or v.Name == "Clans" or v.Name == "Codes" or v.Name == "CustomForceField" or v.Name == "MenuBloodSp"  or v.Name == "PlayerList" then
                                    v:Destroy()
                                end
                            end
                        end
                    }
                end
            }

            cacb()

            func {
                bounty.functions.misc,
                "cp",
                function(i, v)
                    v = v or lp.Character.PrimaryPart.CFrame
                    return (Vector3.new(i.X, 0, i.Z) - Vector3.new(v.X, 0, v.Z))
                end
            }

            func {
                bounty.functions.misc,
                "cd",
                function(i, v)
                    v = v or lp.Character.PrimaryPart.CFrame
                    return (cp(i, v)).Magnitude 
                end
            }

            func {
                bounty.functions.misc,
                "cm",
                function(v)
                    return v and ffc(v.Character, "Humanoid") and v.Character.Humanoid.Health > 0 and ffc(v.Character, "HumanoidRootPart")
                end
            }

            set {"world", ({Zou = 3, Dressrosa = 2, Main = 1})[getsenv(rs.GuideModule)["_G"]["CurrentWorld"]]}

            if world == 1 then
                set {
                    {"distbyp", 1000},
                    {
                        "ps",
                        {
                            Vector3.new(-7894.6201171875, 5545.49169921875, -380.246346191406),
                            Vector3.new(-4607.82275390625, 872.5422973632812, -1667.556884765625),
                            Vector3.new(61163.8515625, 11.759522438049316, 1819.7841796875),
                            Vector3.new(3876.280517578125, 35.10614013671875, -1939.3201904296875)
                        }
                    }
                }
            elseif world == 2 then
                set {
                    {"distbyp", 3500},
                    {
                        "ps",
                        {
                            Vector3.new(-288.46246337890625, 306.130615234375, 597.9988403320312),
                            Vector3.new(2284.912109375, 15.152046203613281, 905.48291015625),
                            Vector3.new(923.21252441406, 126.9760055542, 32852.83203125),
                            Vector3.new(-6508.5581054688, 89.034996032715, -132.83953857422)
                        }
                    }
                }
            else
                set {
                    {"distbyp", 6000},
                    {
                        "ps",
                        {
                            Vector3.new(-5058.77490234375, 314.5155029296875, -3155.88330078125),
                            Vector3.new(5756.83740234375, 610.4240112304688, -253.9253692626953),
                            Vector3.new(-12463.8740234375, 374.9144592285156, -7523.77392578125),
                            Vector3.new(28282.5703125, 14896.8505859375, 105.1042709350586),
                            Vector3.new(-11993.580078125, 334.7812805175781, -8844.1826171875),
                            Vector3.new(5314.58203125, 25.419387817382812, -125.94227600097656)
                        }
                    }
                }
            end

            func {
                bounty.functions.misc,
                "gp",
                function(v)
                    local oldp, dis = Vector3.new(0,0,0), math.huge
                    fl {
                        ps,
                        function(_, p)
                            if cd(v, p) < dis and oldp ~= p then
                                oldp, dis = p, cd(v, p)
                            end
                        end
                    }
                    return oldp
                end
            }

            func {
                bounty.functions.misc,
                "rqet",
                function(v)
                    if tween then
                        tween:Cancel()
                    end
                    wt(0.1)
                    inv(comf, {"requestEntrance", v})
                    wt(0.1)
                end
            }

            func {
                bounty.functions.misc,
                "gbp",
                function(v)
                    local a, b = nil, math.huge
                    for _, c in pairs(ffc(workspace._WorldOrigin.PlayerSpawns, str(lp.Team)):GetChildren()) do
                        if str(c) ~= "Leviathan" and c:GetModelCFrame() ~= a and cd(c:GetModelCFrame(), v) <= b then
                            a = c:GetModelCFrame()
                            b = cd(c:GetModelCFrame(), v)
                        end
                    end
                    return a
                end
            }

            func {
                bounty.functions.misc,
                "cic",
                function(v)
                    if not v then v = lp end
                    return v.Character:GetAttribute("InCombat") == 1
                end
            }

            func {
                bounty.functions.misc,
                "bypass",
                function(v)
                    bounty["can use weapon"] = false
                    if tween then
                        tween:Cancel()
                    end
                    wt(0.1)
                    wfhrp(lp).CFrame = v
                    if cic() then
                        return
                    end
                    wfh(lp):ChangeState(15)
                    if ffc(lp.Character, "Head") then
                        lp.Character.Head:Destroy()
                    end
                    wt(1)
                    repeat wt()
                        lp.Character:SetPrimaryPartCFrame(v)
                    until wfh(lp).Health > 0 and ffc(lp.Character, "Head")
                    wt(0.5)
                end
            }

            func {
                bounty.functions.misc,
                "to",
                function(v)
                    if not ffc(lp.Character, "Humanoid") or wfh(lp).Health <= 0 then return end
                    wfhrp(lp)
                    fl {
                        lp.Character:GetDescendants(),
                        function(i, v)
                            if v:IsA "BasePart" then
                                v.CanCollide = false
                            end
                        end
                    }
                    if not ffc(wfhrp(lp), "Hold") then
                        local Hold = Instance.new("BodyVelocity", wfhrp(lp))
                        Hold.Name = "Hold"
                        Hold.MaxForce = Vector3.new(math.huge, math.huge, math.huge)
                        Hold.Velocity = Vector3.new(0, 0, 0)
                    end
                    wfh(lp).Sit = false
                    Distance = cd(wfhrp(lp).CFrame, v)
                    Portal = gp(v) 
                    Spawn = gbp(v)
                    if gbp(wfhrp(lp).CFrame) ~= Spawn then
                        if cd(Portal, v) < cd(v) and cd(Portal) > 800 then
                            return rqet(Portal)
                        end
                        if not cic() and cd(v) - cd(Spawn, v) > 1200 and cd(Spawn) > 1000 then
                            return bypass(Spawn)
                        end
                    end
                    wfhrp(lp).CFrame = CFrame.new(wfhrp(lp).CFrame.X, v.Y, wfhrp(lp).CFrame.Z)
                    if bounty["tween set cframe"] and Distance <= 100 then
                        wfhrp(lp).CFrame = v
                    end
                    tween = game:GetService("TweenService"):Create(wfhrp(lp), TweenInfo.new(Distance / 350, Enum.EasingStyle.Linear),{CFrame = v})
                    tween:Play() 
                end
            }

            func {
                bounty.functions.misc,
                "hop",
                function(reason)
                    if not reason then reason = "Find New Server to hunt bounty" end
                    if not bounty.hop then
                        bounty.hop = true
                        hopsv = tick()
                        spawn(function()
                            while wt() do
                                if tick() - hopsv > 30 then
                                    game:GetService("TeleportService"):Teleport(game.PlaceId, lp)
                                end
                            end
                        end)
                        local Blur = Instance.new("BlurEffect")
                        Blur.Size = 50
                        Blur.Parent = game.Lighting
                        Blur.Enabled = true 

                        local HopGui = Instance.new("ScreenGui");
                        local HopFrame = Instance.new("Frame");
                        local NameHub = Instance.new("TextLabel");
                        local UIStroke = Instance.new("UIStroke");
                        local HopIn = Instance.new("TextLabel");
                        local DropShadowHolder = Instance.new("Frame");
                        local DropShadow = Instance.new("ImageLabel");
                        local Reason = Instance.new("TextLabel");
                        local ClickTo = Instance.new("TextLabel");

                        HopGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
                        HopGui.Name = "HopGui"
                        HopGui.Parent = coregui

                        HopFrame.AnchorPoint = Vector2.new(0.5, 0.5)
                        HopFrame.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
                        HopFrame.BackgroundTransparency = 0.9990000128746033
                        HopFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
                        HopFrame.BorderSizePixel = 0
                        HopFrame.Position = UDim2.new(0.5, 0, 0.5, -35)
                        HopFrame.Size = UDim2.new(1, 0, 1, 70)
                        HopFrame.Name = "HopFrame"
                        HopFrame.Parent = HopGui

                        NameHub.Font = Enum.Font.Gotham
                        NameHub.Text = "Grape Haven"
                        NameHub.TextColor3 = Color3.fromRGB(175.00000476837158, 187.00000405311584, 230.00000149011612)
                        NameHub.TextSize = 85
                        NameHub.AnchorPoint = Vector2.new(0.5, 0.5)
                        NameHub.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                        NameHub.BackgroundTransparency = 0.9990000128746033
                        NameHub.BorderColor3 = Color3.fromRGB(0, 0, 0)
                        NameHub.BorderSizePixel = 0
                        NameHub.Position = UDim2.new(0.5, 0, 0.5, -45)
                        NameHub.Size = UDim2.new(0, 200, 0, 80)
                        NameHub.Name = "NameHub"
                        NameHub.Parent = HopFrame

                        UIStroke.Color = Color3.fromRGB(175.00000476837158, 187.00000405311584, 230.00000149011612)
                        UIStroke.Thickness = 1.5
                        UIStroke.Parent = NameHub

                        HopIn.Font = Enum.Font.GothamBold
                        HopIn.Text = "Hopping server"
                        HopIn.TextColor3 = Color3.fromRGB(230.00000149011612, 230.00000149011612, 230.00000149011612)
                        HopIn.TextSize = 18
                        HopIn.AnchorPoint = Vector2.new(0.5, 0.5)
                        HopIn.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                        HopIn.BackgroundTransparency = 0.9990000128746033
                        HopIn.BorderColor3 = Color3.fromRGB(0, 0, 0)
                        HopIn.BorderSizePixel = 0
                        HopIn.Position = UDim2.new(0.5, 0, 0.5, 0)
                        HopIn.Size = UDim2.new(0, 200, 0, 30)
                        HopIn.Name = "HopIn"
                        HopIn.Parent = HopFrame

                        DropShadowHolder.BackgroundTransparency = 1
                        DropShadowHolder.BorderSizePixel = 0
                        DropShadowHolder.Size = UDim2.new(1, 0, 1, 0)
                        DropShadowHolder.ZIndex = 0
                        DropShadowHolder.Name = "DropShadowHolder"
                        DropShadowHolder.Parent = HopFrame

                        DropShadow.Image = "rbxassetid://6015897843"
                        DropShadow.ImageColor3 = Color3.fromRGB(0, 0, 0)
                        DropShadow.ImageTransparency = 0.2
                        DropShadow.ScaleType = Enum.ScaleType.Slice
                        DropShadow.SliceCenter = Rect.new(49, 49, 450, 450)
                        DropShadow.AnchorPoint = Vector2.new(0.5, 0.5)
                        DropShadow.BackgroundTransparency = 1
                        DropShadow.BorderSizePixel = 0
                        DropShadow.Position = UDim2.new(0.5, 0, 0.5, 0)
                        DropShadow.Size = UDim2.new(1, 47, 1, 47)
                        DropShadow.ZIndex = 0
                        DropShadow.Name = "DropShadow"
                        DropShadow.Parent = DropShadowHolder

                        Reason.Font = Enum.Font.GothamMedium
                        Reason.Text = "Reason: "..reason
                        Reason.TextColor3 = Color3.fromRGB(230.00000149011612, 230.00000149011612, 230.00000149011612)
                        Reason.TextSize = 16
                        Reason.AnchorPoint = Vector2.new(0.5, 0.5)
                        Reason.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                        Reason.BackgroundTransparency = 0.9990000128746033
                        Reason.BorderColor3 = Color3.fromRGB(0, 0, 0)
                        Reason.BorderSizePixel = 0
                        Reason.Position = UDim2.new(0.5, 0, 0.5, 34)
                        Reason.Size = UDim2.new(0, 200, 0, 16)
                        Reason.Name = "Reason"
                        Reason.Parent = HopFrame

                        ClickTo.Font = Enum.Font.Gotham
                        ClickTo.Text = "https://discord.gg/pdKhtkYRqU"
                        ClickTo.TextColor3 = Color3.fromRGB(255, 255, 255)
                        ClickTo.TextSize = 15
                        ClickTo.TextTransparency = 0.699999988079071
                        ClickTo.AnchorPoint = Vector2.new(0.5, 0.5)
                        ClickTo.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                        ClickTo.BackgroundTransparency = 0.9990000128746033
                        ClickTo.BorderColor3 = Color3.fromRGB(0, 0, 0)
                        ClickTo.BorderSizePixel = 0
                        ClickTo.Position = UDim2.new(0.5, 0, 0.5, 50)
                        ClickTo.Size = UDim2.new(0, 200, 0, 30)
                        ClickTo.Name = "ClickTo"
                        ClickTo.Parent = HopFrame

                        spawn(function()
                            func {
                                bounty.functions.misc,
                                "v1",
                                function(v)
                                    if v.Name == "ErrorPrompt" then
                                        if v.Visible and v.TitleFrame.ErrorTitle.Text == "Teleport Failed" then
                                            v.Visible = false
                                        end
                                        v:GetPropertyChangedSignal("Visible"):Connect(function()
                                            if v.Visible and v.TitleFrame.ErrorTitle.Text == "Teleport Failed" then
                                                v.Visible = false
                                            end
                                        end)
                                    end
                                end
                            }

                            fl {
                                coregui.RobloxPromptGui.promptOverlay:GetChildren(), 
                                function(v)
                                    v1(v) 
                                end
                            }

                            coregui.RobloxPromptGui.promptOverlay.ChildAdded:Connect(v1)
                        end)

                        while wt() do
                            if not cic() then
                                spawn(function()
                                    while wt() do
                                        bypass(CFrame.new(-1903.6856689453125, 36.70722579956055, -11857.265625))
                                    end
                                end)
                                for r = 1, math.huge do
                                    lp.PlayerGui.ServerBrowser.Frame.Filters.SearchRegion.TextBox.Text = config.Misc["Hop Region"]
                                    for k, v in pairs(inv(rs.__ServerBrowser, {r})) do
                                        if k ~= game.JobId and v["Count"] <= 10 then
                                            if not servers[k] then
                                                HopIn.Text = "Hopping to server: "..str(k)
                                                inv(rs.__ServerBrowser, {"teleport", k})
                                            end
                                        end
                                    end
                                    wt()
                                end
                            else
                                hopsv = tick()
                                bounty["tween set cframe"] = true
                                to(CFrame.new(math.random(-500, 500), math.random(-5000, 5000), math.random(-500, 500))) 
                                HopIn.Text = "Waiting Combat off"
                            end
                        end
                    end
                end
            }

            set {
                {"vgun", tick()},
                {"vfast", tick()},
                {"uis", game:GetService("UserInputService")},
                {"mouse", lp:GetMouse()}
            }

            set {"v3", (uis.TouchEnabled and uis.TouchTapInWorld) or mouse.Button1Down}
            
            loadstring(game:HttpGet("https://github.com/ErutTheTeru/script/blob/main/hook-effect.lua?raw=true"))()
            loadstring(game:HttpGet("https://github.com/ErutTheTeru/script/blob/main/blox-fastattack-skid.lua?raw=true"))()
            loadstring(game:HttpGet("https://github.com/ErutTheTeru/script/blob/main/blox-rejoin-skid.lua?raw=true"))()

            func {
                bounty.functions.misc,
                "attack",
                function()
                    if ffc(lp.Character, "Stun") and lp.Character.Stun.Value ~= 0 then
                        return
                    end
                    if bounty["tool tip using"] == "Gun" then
                        if tick() - vgun >= config.Misc["Click Delay"] then
                            for _, v in pairs(getconnections(v3)) do
                                if type(v.Function) == "function" and debug.getinfo(v.Function).name == "inputAndReload" then
                                    v.Function()
                                end
                            end
                            vgun = tick()
                        end
                        if config["Gun Mode"] then
                            if tick() - vfast >= config.Misc["Click Delay"] then
                                click()
                                vfast = tick()
                            end
                        end
                    else
                        if tick() - vfast >= config.Misc["Click Delay"] then
                            click()
                            vfast = tick()
                        end
                    end
                end
            }

            local mainfolder = "Grape Haven"
            local bountyfolder = mainfolder.."/Auto Bounty"
            local mainbounty = bountyfolder.."/Main"
            local themefolder = bountyfolder.."/Theme"

            func {
                bounty.functions.misc,
                "readdata",
                function(filename, customasset, olddata)
                    if not isfolder(mainfolder) then makefolder(mainfolder) end
                    if not isfolder(bountyfolder) then makefolder(bountyfolder) end
                    if not isfolder(themefolder) then makefolder(themefolder) end
                    if not isfolder(mainbounty) then makefolder(mainbounty) end
                    if customasset then 
                        if filename == "" then return "" end
                        filename = themefolder.."/"..filename
                        if isfile(filename) then
                            return getcustomasset(filename)
                        end
                        return
                    end
                    filename = mainbounty.."/"..lp.Name.."_"..filename..".json"
                    if isfile(filename) then
                        return game:GetService("HttpService"):JSONDecode(readfile(filename))
                    end
                    return olddata
                end
            }

            func {
                bounty.functions.misc,
                "save",
                function(filename, filecontent)
                    local filename = mainbounty.."/"..lp.Name.."_"..filename..".json"
                    local filecontent = game:GetService("HttpService"):JSONEncode(filecontent)
                    if not isfolder(mainbounty) then makefolder(mainbounty) end
                    if isfolder(mainbounty) then
                        writefile(filename, filecontent)
                    else
                        makefolder(mainbounty)
                        writefile(filename, filecontent)
                    end
                    return filecontent
                end
            }

            set {"teams", {["Pirates"] = "Bounty", ["Marines"] = "Hornor"}}

            set {"scriptloaded", true}

            set {"servers", readdata("servers", false, {})}

            fl {
                servers,
                function(i, v)
                    if math.floor(((tick() - v) % 3600) / 60) >= 15 then
                        servers[i] = nil
                    end
                end
            }

            if servers[game.JobId] then
                hop("Hunted This Server: "..game.JobId)
            else
                servers[game.JobId] = tick()
            end

            save("servers", servers)

            func {
                bounty.functions.misc,
                "cfruit",
                function(v)
                    local tdf = v and ffc(v, "Data") and ffc(v.Data, 'DevilFruit') and v.Data.DevilFruit.Value
                    for _, a in pairs(config["Skip"]["Fruit"]) do
                        if str(a) == str(tdf) then 
                            return false
                        end
                    end
                    return true
                end
            }

            func {
                bounty.functions.misc,
                "safezone",
                function(v)
                    for _, g in pairs(workspace._WorldOrigin.SafeZones:GetChildren()) do
                        if (v.Position - g.Position).Magnitude <= (ffc(g, "Mesh").Scale.Z / 2) then
                            return false
                        end
                    end
                    return true
                end
            }

            func {
                bounty.functions.misc,
                "targ",
                function()
                    if not bounty.hop then
                        if bounty.targ then
                            bounty.killed = bounty.targ.Name
                            table.insert(bounty.checked, bounty.targ)
                        end
                        if config["Chat Spamming"]["Enabled"] then
                            ffc(wfc(rs, "DefaultChatSystemChatEvents"), "SayMessageRequest"):FireServer(config["Chat Spamming"]["Message"][math.random(1, #config["Chat Spamming"]["Message"])], "All")
                        end
                        bounty["can use weapon"] = false
                        a, b = nil, math.huge
                        fl {
                            plrs:GetChildren(),
                            function(_, v)
                                if v and v ~= lp and v ~= a and v.Team and string.find(str(v.Team.Name), "es") and not table.find(bounty.checked, v)
                                and (str(lp.Team.Name) == "Pirates" or str(v.Team.Name) == "Pirates") and math.abs(lp.Data.Level.Value - v.Data.Level.Value) < 600
                                and cfruit(v) and cd(wfhrp(lp).CFrame, wfhrp(v).CFrame) <= b and not v:GetAttribute("IslandRaiding") and v:GetAttribute("CurrentIsland") ~= "Sea"
                                and (not config["Skip"]["V4"] or not (ffc(v.Backpack, "Awakening") or ffc(v.Character, "Awakening"))) and safezone(wfhrp(v).CFrame)
                                and cd(gbp(wfhrp(v).CFrame).Position, wfhrp(v).Position) <= 3000 and not ffc(v.Character, "TempSafeZone") and cm(v) and wfh(v).MoveDirection.Magnitude <= 0 then
                                    a = v
                                    b = cd(wfhrp(v).CFrame, wfhrp(lp).CFrame)
                                end
                            end
                        }
                        if a ~= nil then
                            noti("New target: "..a.Name)
                            bounty.targ = a
                            return true
                        end
                        hop()
                    end
                    return false
                end
            }

            set {
                {"oldtarg", nil},
                {"oldspamskill", tick()},
                {"oldtargtick", tick()},
                {"startuse", false},
            }

            func {
                bounty.functions.misc,
                "ctarg",
                function()
                    if bounty.hop then 
                        return false
                    end
                    if not cm(bounty.targ) then
                        noti("Player Died")
                        return targ()
                    end
                    if bounty.targ:GetAttribute("IslandRaiding") then
                        noti("Player is Raiding")
                        return targ()
                    end
                    if wfhrp(bounty.targ).CFrame.Y > distbyp then
                        noti("Player is too hard to kill")
                        return targ()
                    end
                    if bounty.targ == oldtarg then
                        if tick() - oldtargtick >= config["Skip"]["Time User"] then
                            noti("Player used too much time to kill")
                            return targ()
                        end
                        if bounty["using skill"] and bounty["can use weapon"] and not startuse then
                            oldspamskill = tick()
                            startuse = true
                        else
                            if startuse and (tick() - oldspamskill) >= 3 and not (bounty.targ.Character:GetAttribute("InCombat") == 0 or bounty.targ.Character:GetAttribute("InCombat") == 1) then
                                noti("Player not able to kill")
                                return targ()
                            end
                        end
                    else
                        startuse = false
                        oldtarg = bounty.targ
                        oldtargtick = tick()
                    end
                    return true
                end
            }

            set {
                {"oldtw", tick()},
                {"r", 30},
                {"v6", 0},
            }

            func {
                bounty.functions.misc,
                "circletween",
                function(v)
                    v6 = v6 + 20
                    return v + Vector3.new(math.sin(math.rad(v6)) * r, 0, math.cos(math.rad(v6)) * r)
                end
            }

            set {"team", join(config["Default Team"])} 

            if config["Auto Team"].Enabled then
                wt(1)
                if (lp.leaderstats["Bounty/Honor"].Value < config["Auto Team"].Lock[teams[team]][1]) or lp.leaderstats["Bounty/Honor"].Value >= config["Auto Team"].Lock[teams[team]][2] then
                    wt(10)
                    for i, v in pairs(teams) do
                        if i ~= team then
                            v5 = i
                        end
                    end
                    team = join(save("auto team", v5))
                    wt(1)
                    if (lp.leaderstats["Bounty/Honor"].Value < config["Auto Team"].Lock[teams[team]][1]) or (lp.leaderstats["Bounty/Honor"].Value >= config["Auto Team"].Lock[teams[team]][2]) then
                        lp:Kick("Lock Bounty / Hornor")
                    end
                end
            end

            set {"anim", Instance.new("Animation")}
            anim.AnimationId = "http://www.roblox.com/asset/?id=1ruahub"

            func {
                bounty.functions.misc,
                "send",
                function()
                    return wfh(lp):LoadAnimation(anim):Play()
                end 
            }

            func {
                bounty.functions.misc,
                "listen",
                function(v, dosth)
                    if cm(v) then
                        print(v.Name)
                        wfh(v).AnimationPlayed:Connect(function(ani) 
                            if tostring(ani.Animation.AnimationId):find("ruahub") then
                                print(v.Name)
                                dosth()
                            end
                        end)
                    end
                end
            }

            fl {
                plrs:GetChildren(),
                function(i, v)
                    if v ~= lp then
                        listen(v, function()
                            set {"detect", true}
                        end)
                    end
                end
            }

            send()
            wt(1)

            if detect then
                hop("Detect Grape Haven Auto Bounty User")
            else
                fl {
                    plrs:GetChildren(),
                    function(i, v)
                        if v ~= lp then
                            listen(v, send)
                        end
                    end
                }
                plrs.PlayerAdded:Connect(function(v)
                    listen(v, send)
                end)
            end

            if config.Misc["Showcase Mode"] then 
                fl {
                    {"RaceEnergy","Compass","Energy","AlliesButton","Code","CrewButton","HomeButton",
                        "Mute","Settings","MenuButton","Beli","Fragments","Level"},
                    function (i, v)
                        lp.PlayerGui.Main[v].Visible = false
                    end
                }
                for i, v in getconnections(coregui.FluxusAndroidUI.LeftBarFrame.Logo.MouseButton1Down) do 
                    v.Function()
                end
            end

            if config["Misc"]["Hide Map"] then
                fl {
                    workspace:GetDescendants(),
                    function(i, v)
                        pcall(function()
                            v.Transparency = 1
                        end)
                    end
                }

                fl {
                    getnilinstances(),
                    function(i, v)
                        pcall(function()
                            v.Transparency = 1
                            fl {
                                v:GetDescendants(),
                                function(i1, v1)
                                    v1.Transparency = 1
                                end
                            }
                        end)
                    end
                }

                workspace.DescendantAdded:Connect(function(v)
                    pcall(function()
                        v.Transparency = 1
                    end)
                end)
            end

            CamFarm = config.Misc["Cam Farm"]

            if not CamFarm then
                lp.CameraMinZoomDistance = 127
                lp.CameraMaxZoomDistance = 127
                lp.CameraMaxZoomDistance = 127
                lp.CameraMinZoomDistance = 0
            else
                lp.CameraMinZoomDistance = 0
                lp.CameraMaxZoomDistance = 0
            end

            spawn(function() game:GetService("RunService"):Set3dRenderingEnabled(not config.Misc["White Screen"]) end)

            set {"oldctarg", CFrame.new(0, 0, 0)}

            func {
                bounty.functions.main,
                "main tween",
                function()
                    rstep:Connect(function()
                        if ctarg() and ffc(lp.Character, "Humanoid") and wfh(lp).Health and int(wfh(lp).Health) > 0 then
                            if wfhrp(lp).CFrame.Y > 110000 and ffc(workspace._WorldOrigin.VArenaDimension, "PortalEye") then
                                to(workspace._WorldOrigin.VArenaDimension.PortalEye.CFrame)
                            else
                                if (int(wfh(lp).Health) < (config["Panic % Health"][1] / 100 * int(wfh(lp).MaxHealth)))
                                or (hide and int(wfh(lp).Health) < (config["Panic % Health"][2] / 100 * int(wfh(lp).MaxHealth))) then
                                    bounty["can use weapon"] = false
                                    hide = true
                                    to(CFrame.new(wfhrp(bounty.targ).Position + Vector3.new(0, math.random(1000, 5000), 0)))
                                else
                                    bounty["cframe target"] = wfhrp(bounty.targ).CFrame
                                    bounty["targ busy"] = ffc(bounty.targ.Character, "Busy") and bounty.targ.Character.Busy.Value
                                    if tick() - oldtw >= 0.15 then
                                        hide = false
                                        if wfh(bounty.targ).MoveDirection.Magnitude > 0 then
                                            bounty["tween set cframe"] = false
                                            bounty.digit = bounty["cframe target"] + wfhrp(bounty.targ).Velocity / 2
                                        else
                                            bounty["tween set cframe"] = true
                                            if (bounty["cframe target"].Position - oldctarg.Position).Magnitude <= 100 then
                                                bounty.digit = bounty["cframe target"] + (bounty["cframe target"].Position - oldctarg.Position)
                                            else
                                                bounty.digit = bounty["cframe target"] 
                                            end
                                            oldctarg = bounty["cframe target"]
                                        end
                                        if config["Hunt Method"]["Use Move Predict"] then
                                            bounty["tween cframe"] = bounty.digit
                                        else
                                            bounty["tween set cframe"] = true
                                            bounty["tween cframe"] = bounty["cframe target"] 
                                            oldtw = tick() - 0.15
                                        end
                                        if (wfhrp(lp).Position - bounty["cframe target"].Position).Magnitude <= 100 then
                                            bounty["can use weapon"] = true
                                        else
                                            bounty["can use weapon"] = false
                                        end
                                        if config["Hunt Method"]["Random X Y Z"] then
                                            bounty["tween cframe"] = bounty["tween cframe"] + Vector3.new(math.random(-15, 15), math.random(-15, 15), math.random(-15, 15))
                                        end
                                        if config["Hunt Method"]["Hit and Run"] then
                                            if not bounty["using skill"] then
                                                bounty["tween cframe"] = bounty["tween cframe"] + Vector3.new(0, 46, 0)
                                            end
                                            if bounty["targ busy"] then
                                                bounty["tween cframe"] = circletween(bounty["tween cframe"])
                                            end
                                        end
                                        bounty["tween cframe"] = bounty["tween cframe"] + Vector3.new(0, 4, 4)
                                        if bounty["tween cframe"].Y <= 5 then
                                            bounty["tween cframe"] = CFrame.new(bounty["tween cframe"].X, 5, bounty["tween cframe"].Z)
                                        end
                                        to(bounty["tween cframe"])
                                        oldtw = tick()
                                    end
                                end
                            end
                        else
                            bounty["can use weapon"] = false
                        end
                    end)
                end
            }

            Cac = hookmetamethod(game, "__index", newcclosure(function(self, Index)
                if self == mouse then
                    if Index == "Hit" or Index == "hit" then
                        return bounty["cframe target"]
                    end
                end
                return Cac(self, Index)
            end))

            spawn(function()
                local gg = getrawmetatable(game)
                local old = gg.__namecall
                setreadonly(gg, false)
                gg.__namecall = newcclosure(function(...)
                    local method = getnamecallmethod()
                    local args = {...}
                    if tostring(method) == "FireServer" then
                        if tostring(args[1]) == "RemoteEvent" then
                            if tostring(args[2]) ~= "true" and tostring(args[2]) ~= "false" then
                                args[2] = bounty.digit.Position
                                return old(unpack(args))
                            end
                        end
                    end
                    return old(...)
                end)
            end)

            func {
                bounty.functions.misc,
                "cwea",
                function(tool)
                    for _, v in pairs(lp.Backpack:GetChildren()) do 
                        if v:IsA("Tool") and v.ToolTip == tool then
                            return v
                        end 
                    end
                    for _, v in pairs(lp.Character:GetChildren()) do 
                        if v:IsA("Tool") and v.ToolTip == tool then
                            return v
                        end 
                    end
                    return cwea(bounty["tool tip using"])
                end
            }

            func {
                bounty.functions.misc,
                "equip",
                function(tool)
                    wea = cwea(tool)
                    if wea then
                        wfh(lp):EquipTool(wea)
                        return wea.Name
                    end
                    return cwea(bounty["tool tip using"]).Name
                end
            }

            func {
                bounty.functions.misc,
                "down",
                function(i, v)
                    if not ffc(lp.Character, bounty["weapon using"]) then
                        bounty["weapon using"] = equip(bounty["tool tip using"])
                        wt(0.1)
                    end
                    vim:SendKeyEvent(true, i, false, game)
                    local spam = tick()
                    repeat wt()
                    until (tick() - spam) >= v or not bounty["can use weapon"] or not (ffc(lp.Character, "Busy") and lp.Character.Busy.Value)
                    vim:SendKeyEvent(false, i, false, game)
                end
            }

            set {
                "races",
                { 
                    ["Human"] = "Last Resort", 
                    ["Mink"] = "Agility", 
                    ["Fishman"] = "Water Body", 
                    ["Skypea"] = "Heavenly Blood", 
                    ["Ghoul"] = "Heightened Senses",
                    ["Cyborg"] = "Energy Core"
                } 
            }

            set {"fastattack", false}

            func {
                bounty.functions.main,
                "main skill",
                function()
                    spawn(function()
                        while wt() do
                            if not ffc(lp.Character, "HasBuso") then
                                inv(comf, {"Buso"})
                            end
                            if ffc(lp, "PlayerGui") and ffc(lp.PlayerGui, "ScreenGui") and ffc(lp.PlayerGui.ScreenGui, "ImageLabel") then
                            else
                                game:service("VirtualUser"):CaptureController()
                                game:service("VirtualUser"):SetKeyDown("0x65")
                                game:service("VirtualUser"):SetKeyUp("0x65")
                            end
                            if ffc(lp.Character, "RaceTransformed") and ffc(lp.Character, "RaceEnergy") and lp.Character.RaceEnergy.Value >= 1 and not lp.Character.RaceTransformed.Value then
                                down("Y", 0)
                            end
                            if lp.PlayerGui.Main.PvpDisabled.Visible == true then
                                rs.Remotes.CommF_:InvokeServer("EnablePvp")
                            end
                            if bounty["can use weapon"] then
                                if not ffc(lp.Character, races[lp.Data.Race.Value]) and inv(comf, {"Wenlocktoad", "1"}) == -2 then
                                    down("T", 0)
                                end
                                if config["Gun Mode"] then
                                    cwea("Melee").Parent = lp.Character
                                    gund = cwea("Gun")
                                    if gund then
                                        bounty["tool tip using"] = "Gun"
                                        bounty["weapon using"] = gund.Name
                                        gund.Parent = lp.Character
                                    end
                                    bounty["using skill"] = true
                                    for i, v in pairs({"Z", "C", "X"}) do
                                        down(v, 0)
                                    end
                                    fastattack = true
                                    wt(0.4)
                                    bounty["using skill"] = false
                                    fastattack = false
                                else
                                    fastattack = false
                                    if config["Macro"].Enabled then
                                        for i, v in pairs(config["Macro"][1]) do
                                            if config.Items[v[1]].Enabled[1] or (config.Items[v[1]].Enabled[2] and config["Spam All Skill On V4"] and ffc(lp.Character, "RaceTransformed") and lp.Character.RaceTransformed.Value) then
                                                bounty["tool tip using"] = v[1]
                                                bounty["weapon using"] = equip(v[1])
                                                wt(0.1)
                                                for _, k in pairs(v[2]) do
                                                    if config.Items[v[1]][k] and config.Items[v[1]][k][1] and ffc(lp.PlayerGui.Main.Skills[bounty["weapon using"]], k) 
                                                    and ((ffc(lp.Character, "Heightened Senses") and lp.PlayerGui.Main.Skills[bounty["weapon using"]][k].Cooldown.Size.X.Scale <= 0.4)
                                                    or lp.PlayerGui.Main.Skills[bounty["weapon using"]][k].Cooldown.Size.X.Scale <= 0) then
                                                        bounty["using skill"] = true
                                                        down(k, config.Items[v[1]][k][2])
                                                        bounty["using skill"] = false
                                                        wt(config.Items[v[1]][k])
                                                        bounty["using skill"] = true
                                                    end
                                                end
                                                if (bounty["using skill"] or v[1] == "Gun") and v[1] ~= "Blox Fruit" then
                                                    fastattack = true
                                                    wt(0.4)
                                                    bounty["using skill"] = false
                                                    fastattack = false
                                                end
                                            end
                                        end
                                    else
                                        for i, v in pairs(config["Items"]) do
                                            if v.Enabled[1] or (v.Enabled[2] and config["Spam All Skill On V4"] and ffc(lp.Character, "RaceTransformed") and lp.Character.RaceTransformed.Value) then
                                                bounty["tool tip using"] = i
                                                bounty["weapon using"] = equip(i)
                                                wt(0.1)
                                                for j, k in pairs(v) do
                                                    if k and k[1] and ffc(lp.PlayerGui.Main.Skills[bounty["weapon using"]], j) 
                                                    and ((ffc(lp.Character, "Heightened Senses") and lp.PlayerGui.Main.Skills[bounty["weapon using"]][j].Cooldown.Size.X.Scale <= 0.4)
                                                    or lp.PlayerGui.Main.Skills[bounty["weapon using"]][j].Cooldown.Size.X.Scale <= 0) then
                                                        bounty["using skill"] = true
                                                        down(j, config.Items[i][j][2])
                                                        bounty["using skill"] = false
                                                        wt(config.Items[i][j])
                                                        bounty["using skill"] = true
                                                    end
                                                end
                                                if (bounty["using skill"] or i == "Gun") and i ~= "Blox Fruit" then
                                                    fastattack = true
                                                    wt(0.4)
                                                    bounty["using skill"] = false
                                                    fastattack = false
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end)
                end
            }

            set {"camfarmtick", tick()}

            func {
                bounty.functions.main,
                "attack while",
                function()
                    rstep:Connect(function()
                        if fastattack then
                            attack()
                        else
                            if config["Misc"]["Cam Farm"] and (tick() - camfarmtick >= 0.15) then
                                workspace.CurrentCamera.CFrame = CFrame.new(workspace.CurrentCamera.CFrame.Position, bounty["cframe target"].Position)
                                camfarmtick = tick()
                            end
                        end
                    end)
                end
            }

            set {"u5", require(rs:WaitForChild("Notification"))}

            notitick = 0
            func {
                bounty.functions.main,
                "notify while",
                function()
                    rstep:Connect(function()
                        if tick() - notitick >= 20 then
                            u5.new("<Color=Yellow>Report bugs Or Suggests Dms<Color=/>"):Display()
                            u5.new("<Color=Yellow>Discord: teru 特鲁<Color=/>"):Display() 
                            u5.new("<Color=Yellow>ID Discord: 1095663105670926448<Color=/>"):Display() 
                            u5.new("<Color=Yellow>Server Discord: discord.gg/CejANXn8sa<Color=/>"):Display() 
                            u5.new("<Color=Yellow>Join Server Check Update<Color=/>"):Display()
                            notitick = tick()
                        end
                    end)
                end
            }

            local UserInputService = game:GetService("UserInputService")
            local TweenService = game:GetService("TweenService")

            if counter.Enabled then
                func {
                    bounty.functions.misc,
                    "embed",
                    function(v) 
                        return '```' .. str(v) .. '```'
                    end
                }

                func {
                    bounty.functions.misc,
                    "sendwebhook",
                    function(...)
                        v = {...}
                        local Request = http_request or request or HttpPost or syn.request
                        local Data = {
                            Url = counter.Webhook.Url, 
                            Body = game:GetService("HttpService"):JSONEncode(
                                {
                                    ["content"] = "",
                                    ["embeds"] = {
                                        {
                                            ["title"] = "**Grape Haven**",
                                            ['color'] = 8631807,
                                            ["fields"] = {
                                                {
                                                    ['name'] = "Username",
                                                    ['value'] = embed(lp.Name),
                                                    ['inline'] = true
                                                },
                                                {
                                                    ['name'] = "Killed",
                                                    ['value'] = embed(str(v[1])),
                                                    ['inline'] = true
                                                },
                                                {
                                                    ['name'] = "Total Bounty Earned",
                                                    ['value'] = embed(str(v[2]):reverse():gsub("%d%d%d", "%1,"):reverse():gsub("^,", "")),
                                                    ['inline'] = false
                                                },
                                                {
                                                    ['name'] = "Total Time Elapsed",
                                                    ['value'] = embed(str(v[3])),
                                                    ['inline'] = false
                                                },
                                            },
                                            ['thumbnail'] = {
                                                ['url'] = "https://cdn.discordapp.com/attachments/1283711478012186668/1283766673563979829/img.png?ex=66e43062&is=66e2dee2&hm=7b989bc2ae6528fb1658b0196eb647d4d9cc6ee10402397d7ad265b7c2fac6a1&"
                                            },
                                            ['footer'] = {
                                                ['text'] = 'Grape Haven - discord.gg/pdKhtkYRqU',
                                            },
                                            ['timestamp'] = os.date('!%Y-%m-%dT%H:%M:%SZ')
                                        }
                                    }
                                }
                            ), 
                            Method = "POST", 
                            Headers = {["content-type"] = "application/json"}
                        }
                        Request(Data)
                    end
                }

                getgenv().CounterSave = {
                    ["Total Earned"] = 0,
                    ["Total Time Count"] = 0
                }

                local function MakeDraggable(topbarobject, object)
                    local Dragging = nil
                    local DragInput = nil
                    local DragStart = nil
                    local StartPosition = nil

                    local function UpdatePos(input)
                        local Delta = input.Position - DragStart
                        local pos = UDim2.new(StartPosition.X.Scale, StartPosition.X.Offset + Delta.X, StartPosition.Y.Scale, StartPosition.Y.Offset + Delta.Y)
                        local Tween = TweenService:Create(object, TweenInfo.new(0.2), {Position = pos})
                        Tween:Play()
                    end

                    topbarobject.InputBegan:Connect(function(input)
                        if input.UserInputType == Enum.UserInputType.MouseButton1 or input.UserInputType == Enum.UserInputType.Touch then
                            Dragging = true
                            DragStart = input.Position
                            StartPosition = object.Position

                            input.Changed:Connect(function()
                                if input.UserInputState == Enum.UserInputState.End then
                                    Dragging = false
                                end
                            end)
                        end
                    end)

                    topbarobject.InputChanged:Connect(function(input)
                        if input.UserInputType == Enum.UserInputType.MouseMovement or input.UserInputType == Enum.UserInputType.Touch then
                            DragInput = input
                        end
                    end)

                    UserInputService.InputChanged:Connect(function(input)
                        if input == DragInput and Dragging then
                            UpdatePos(input)
                        end
                    end)
                end

                func {
                    bounty.functions.misc,
                    "totaltime",
                    function(starttime, saveit)
                        local elapsed = tick() - starttime
                        local hours = math.floor(elapsed / 3600)
                        local minutes = math.floor((elapsed % 3600) / 60)
                        local seconds = math.floor(elapsed % 60)
                        if saveit then
                            CounterSave["Total Time Count"] = elapsed
                        end
                        local formattedTime = string.format("%dh:%dm:%ds", hours, minutes, seconds)
                        return formattedTime
                    end
                }

                getgenv().CounterSave = readdata("counter", false, CounterSave)

                getgenv().CounterNotSave = {
                    ["Old Bounty"] = lp.leaderstats["Bounty/Honor"].Value,
                    ["Current Bounty"] = lp.leaderstats["Bounty/Honor"].Value,
                    ["Earned"] = 0,
                    ["Earned2"] = 0,
                    ["Another Earned"] = 0,
                    ["Old Total Earned"] = CounterSave["Total Earned"],
                    ["Cilent Time Count"] = tick(),
                    ["Account Time Count"] = tick() - CounterSave["Total Time Count"]
                }

                if counter.Theme.Enabled then
                    local ThemeTable = loadstring(game:HttpGet("https://github.com/ErutTheTeru/script/blob/main/blox-bountytheme.lua?raw=true"))()
                    local ThemeAsset = ThemeTable[counter.Theme["Theme Character"]]

                    if not ThemeAsset then
                        ThemeAsset = ThemeTable["Yae"]
                    end
                    if counter.Theme.Custom.Enabled then
                        for i, v in counter.Theme.Custom["File Config"] do
                            if i ~= "Color" then
                                assettheme = readdata(v, true, "")
                                if assettheme then
                                    ThemeAsset[i] = assettheme
                                end
                            else
                                ThemeAsset[i] = v
                            end
                        end
                    end

                    local BountyGui = Instance.new("ScreenGui");
                    local MainFrame = Instance.new("Frame");
                    local UICorner = Instance.new("UICorner");
                    local Background = Instance.new("ImageLabel");
                    local UICorner1 = Instance.new("UICorner");
                    local Character = Instance.new("ImageLabel");
                    local TextLabel = Instance.new("TextLabel");
                    local TextLabel1 = Instance.new("TextLabel");
                    local CharacterIcon = Instance.new("ImageLabel");
                    local BountyEarned = Instance.new("TextLabel");
                    local UIStroke = Instance.new("UIStroke");
                    local TotalBountyEarned = Instance.new("TextLabel");
                    local UIStroke1 = Instance.new("UIStroke");
                    local CilentTimeElapsed = Instance.new("TextLabel");
                    local UIStroke2 = Instance.new("UIStroke");
                    local AccountTimeElapsed = Instance.new("TextLabel");
                    local UIStroke3 = Instance.new("UIStroke");
                    local ResetImg = Instance.new("ImageLabel");
                    local UICorner2 = Instance.new("UICorner");
                    local ResetButton = Instance.new("TextButton");
                    local UIStroke4 = Instance.new("UIStroke");
                    local UIStroke5 = Instance.new("UIStroke");
                    local CrentBounty = Instance.new("TextLabel");
                    local UIStroke6 = Instance.new("UIStroke");
                    local UIStroke7 = Instance.new("UIStroke");
                    local SkipImg = Instance.new("ImageLabel");
                    local UICorner3 = Instance.new("UICorner");
                    local SkipButton = Instance.new("TextButton");
                    local UIStroke8 = Instance.new("UIStroke");
                    local UIStroke9 = Instance.new("UIStroke");

                    BountyGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
                    BountyGui.Name = "BountyGui"
                    BountyGui.Parent = coregui

                    MainFrame.AnchorPoint = Vector2.new(0.5, 0.5)
                    MainFrame.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
                    MainFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    MainFrame.BorderSizePixel = 0
                    MainFrame.Position = UDim2.new(0.5, 0, 0.5, 0)
                    MainFrame.Size = UDim2.new(0, 750, 0, 380)
                    MainFrame.Name = "MainFrame"
                    MainFrame.Parent = BountyGui

                    UICorner.CornerRadius = UDim.new(0, 5)
                    UICorner.Parent = MainFrame

                    Background.Image = ThemeAsset["Background"]
                    Background.AnchorPoint = Vector2.new(0.5, 0.5)
                    Background.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Background.BackgroundTransparency = 0.9990000128746033
                    Background.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    Background.BorderSizePixel = 0
                    Background.Position = UDim2.new(0.5, 0, 0.5, 0)
                    Background.Size = UDim2.new(1, 0, 1, 0)
                    Background.Name = "Background"
                    Background.Parent = MainFrame

                    UICorner1.CornerRadius = UDim.new(0, 5)
                    UICorner1.Parent = Background

                    Character.Image = ThemeAsset["Character"]
                    Character.AnchorPoint = Vector2.new(1, 1)
                    Character.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    Character.BackgroundTransparency = 0.9990000128746033
                    Character.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    Character.BorderSizePixel = 0
                    Character.Position = UDim2.new(1, 0, 1, 0)
                    Character.Size = UDim2.new(0, 380, 0, 460)
                    Character.Name = "Character"
                    Character.Parent = MainFrame

                    TextLabel.Font = Enum.Font.GrenzeGotisch
                    TextLabel.Text = "Grape Haven Auto Bounty"
                    TextLabel.TextColor3 = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    TextLabel.TextSize = 51
                    TextLabel.TextXAlignment = Enum.TextXAlignment.Left
                    TextLabel.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
                    TextLabel.BackgroundTransparency = 0.9990000128746033
                    TextLabel.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    TextLabel.BorderSizePixel = 0
                    TextLabel.Position = UDim2.new(0, 74, 0, 9)
                    TextLabel.Size = UDim2.new(0, 320, 0, 45)
                    TextLabel.Parent = MainFrame

                    TextLabel1.Font = Enum.Font.GrenzeGotisch
                    TextLabel1.Text = "Grape Haven Auto Bounty"
                    TextLabel1.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    TextLabel1.TextSize = 51
                    TextLabel1.TextXAlignment = Enum.TextXAlignment.Left
                    TextLabel1.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
                    TextLabel1.BackgroundTransparency = 0.9990000128746033
                    TextLabel1.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    TextLabel1.BorderSizePixel = 0
                    TextLabel1.Position = UDim2.new(0, 2, 0, -2)
                    TextLabel1.Size = UDim2.new(1, 0, 1, 0)
                    TextLabel1.Parent = TextLabel

                    CharacterIcon.Image = ThemeAsset["Icon"]
                    CharacterIcon.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    CharacterIcon.BackgroundTransparency = 0.9990000128746033
                    CharacterIcon.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    CharacterIcon.BorderSizePixel = 0
                    CharacterIcon.Position = UDim2.new(0, 6, 0, 6)
                    CharacterIcon.Size = UDim2.new(0, 50, 0, 50)
                    CharacterIcon.Name = "CharacterIcon"
                    CharacterIcon.Parent = MainFrame

                    BountyEarned.Font = Enum.Font.Bodoni
                    BountyEarned.Text = "Bounty Earned: 0$"
                    BountyEarned.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    BountyEarned.TextSize = 25
                    BountyEarned.TextTransparency = 0.10000000149011612
                    BountyEarned.TextXAlignment = Enum.TextXAlignment.Left
                    BountyEarned.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    BountyEarned.BackgroundTransparency = 0.9990000128746033
                    BountyEarned.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    BountyEarned.BorderSizePixel = 0
                    BountyEarned.Position = UDim2.new(0, 15, 0, 123)
                    BountyEarned.Size = UDim2.new(0, 336, 0, 27)
                    BountyEarned.Name = "BountyEarned"
                    BountyEarned.Parent = MainFrame

                    UIStroke.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke.Thickness = 0.800000011920929
                    UIStroke.Parent = BountyEarned

                    TotalBountyEarned.Font = Enum.Font.Bodoni
                    TotalBountyEarned.Text = "Total Bounty Earned: 0$"
                    TotalBountyEarned.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    TotalBountyEarned.TextSize = 25
                    TotalBountyEarned.TextTransparency = 0.10000000149011612
                    TotalBountyEarned.TextXAlignment = Enum.TextXAlignment.Left
                    TotalBountyEarned.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    TotalBountyEarned.BackgroundTransparency = 0.9990000128746033
                    TotalBountyEarned.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    TotalBountyEarned.BorderSizePixel = 0
                    TotalBountyEarned.Position = UDim2.new(0, 15, 0, 175)
                    TotalBountyEarned.Size = UDim2.new(0, 336, 0, 27)
                    TotalBountyEarned.Name = "TotalBountyEarned"
                    TotalBountyEarned.Parent = MainFrame

                    UIStroke1.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke1.Thickness = 0.800000011920929
                    UIStroke1.Parent = TotalBountyEarned

                    CilentTimeElapsed.Font = Enum.Font.Bodoni
                    CilentTimeElapsed.Text = "Cilent Time Elapsed: 0h:0m:0s"
                    CilentTimeElapsed.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    CilentTimeElapsed.TextSize = 25
                    CilentTimeElapsed.TextTransparency = 0.10000000149011612
                    CilentTimeElapsed.TextXAlignment = Enum.TextXAlignment.Left
                    CilentTimeElapsed.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    CilentTimeElapsed.BackgroundTransparency = 0.9990000128746033
                    CilentTimeElapsed.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    CilentTimeElapsed.BorderSizePixel = 0
                    CilentTimeElapsed.Position = UDim2.new(0, 15, 0, 228)
                    CilentTimeElapsed.Size = UDim2.new(0, 336, 0, 27)
                    CilentTimeElapsed.Name = "CilentTimeElapsed"
                    CilentTimeElapsed.Parent = MainFrame

                    UIStroke2.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke2.Thickness = 0.800000011920929
                    UIStroke2.Parent = CilentTimeElapsed

                    AccountTimeElapsed.Font = Enum.Font.Bodoni
                    AccountTimeElapsed.Text = "Account Time Elapsed: 0h:0m:0s"
                    AccountTimeElapsed.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    AccountTimeElapsed.TextSize = 25
                    AccountTimeElapsed.TextTransparency = 0.10000000149011612
                    AccountTimeElapsed.TextXAlignment = Enum.TextXAlignment.Left
                    AccountTimeElapsed.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    AccountTimeElapsed.BackgroundTransparency = 0.9990000128746033
                    AccountTimeElapsed.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    AccountTimeElapsed.BorderSizePixel = 0
                    AccountTimeElapsed.Position = UDim2.new(0, 15, 0, 281)
                    AccountTimeElapsed.Size = UDim2.new(0, 336, 0, 27)
                    AccountTimeElapsed.Name = "AccountTimeElapsed"
                    AccountTimeElapsed.Parent = MainFrame

                    UIStroke3.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke3.Thickness = 0.800000011920929
                    UIStroke3.Parent = AccountTimeElapsed

                    ResetImg.Image = ThemeAsset["Button Image"]
                    ResetImg.AnchorPoint = Vector2.new(0, 1)
                    ResetImg.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    ResetImg.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    ResetImg.BorderSizePixel = 0
                    ResetImg.Position = UDim2.new(0, 8, 1, -8)
                    ResetImg.Size = UDim2.new(0, 150, 0, 48)
                    ResetImg.Name = "ResetImg"
                    ResetImg.Parent = MainFrame

                    UICorner2.CornerRadius = UDim.new(0, 3)
                    UICorner2.Parent = ResetImg

                    ResetButton.Font = Enum.Font.Bodoni
                    ResetButton.Text = "Reset"
                    ResetButton.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    ResetButton.TextSize = 30
                    ResetButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    ResetButton.BackgroundTransparency = 0.9990000128746033
                    ResetButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    ResetButton.BorderSizePixel = 0
                    ResetButton.Size = UDim2.new(1, 0, 1, 0)
                    ResetButton.Name = "ResetButton"
                    ResetButton.Parent = ResetImg

                    UIStroke4.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke4.Thickness = 0.800000011920929
                    UIStroke4.Parent = ResetButton

                    UIStroke5.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke5.Thickness = 1.7999999523162842
                    UIStroke5.Parent = ResetImg

                    CrentBounty.Font = Enum.Font.Bodoni
                    CrentBounty.Text = "Current Bounty: 0$"
                    CrentBounty.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    CrentBounty.TextSize = 25
                    CrentBounty.TextTransparency = 0.10000000149011612
                    CrentBounty.TextXAlignment = Enum.TextXAlignment.Left
                    CrentBounty.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    CrentBounty.BackgroundTransparency = 0.9990000128746033
                    CrentBounty.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    CrentBounty.BorderSizePixel = 0
                    CrentBounty.Position = UDim2.new(0, 14, 0, 70)
                    CrentBounty.Size = UDim2.new(0, 336, 0, 27)
                    CrentBounty.Name = "CrentBounty"
                    CrentBounty.Parent = MainFrame

                    UIStroke6.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke6.Thickness = 0.800000011920929
                    UIStroke6.Parent = CrentBounty

                    UIStroke7.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke7.Thickness = 1.7999999523162842
                    UIStroke7.Parent = MainFrame

                    SkipImg.Image = ThemeAsset["Button Image"]
                    SkipImg.AnchorPoint = Vector2.new(0, 1)
                    SkipImg.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    SkipImg.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    SkipImg.BorderSizePixel = 0
                    SkipImg.Position = UDim2.new(0, 166, 1, -8)
                    SkipImg.Size = UDim2.new(0, 150, 0, 48)
                    SkipImg.Name = "SkipImg"
                    SkipImg.Parent = MainFrame

                    UICorner3.CornerRadius = UDim.new(0, 3)
                    UICorner3.Parent = SkipImg

                    SkipButton.Font = Enum.Font.Bodoni
                    SkipButton.Text = "Next Player"
                    SkipButton.TextColor3 = Color3.fromRGB(221.00000202655792, 86.00000247359276, 92.00000211596489)
                    SkipButton.TextSize = 30
                    SkipButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    SkipButton.BackgroundTransparency = 0.9990000128746033
                    SkipButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    SkipButton.BorderSizePixel = 0
                    SkipButton.Size = UDim2.new(1, 0, 1, 0)
                    SkipButton.Name = "SkipButton"
                    SkipButton.Parent = SkipImg

                    UIStroke8.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke8.Thickness = 0.800000011920929
                    UIStroke8.Parent = SkipButton

                    UIStroke9.Color = Color3.fromRGB(104.00000140070915, 47.0000009983778, 46.000001057982445)
                    UIStroke9.Thickness = 1.7999999523162842
                    UIStroke9.Parent = SkipImg

                    SkipButton.Activated:Connect(function()
                        if bounty.targ then
                            noti("Skiped Player: "..bounty.targ.Name)
                        end
                        targ()
                    end)

                    ResetButton.Activated:Connect(function()
                        noti("Reset Counter Successfull!")
                        getgenv().CounterSave = {
                            ["Total Time Count"] = 0,
                            ["Total Earned"] = 0
                        }
                        getgenv().CounterNotSave = {
                            ["Old Bounty"] = lp.leaderstats["Bounty/Honor"].Value,
                            ["Current Bounty"] = lp.leaderstats["Bounty/Honor"].Value,
                            ["Earned"] = 0,
                            ["Another Earned"] = 0,
                            ["Old Total Earned"] = CounterSave["Total Earned"],
                            ["Cilent Time Count"] = tick(),
                            ["Account Time Count"] = tick() - CounterSave["Total Time Count"]
                        }
                        save("counter", CounterSave)
                    end)

                    func {
                        bounty.functions.main,
                        "main counter",
                        function()
                            spawn(function()
                                while wt() do
                                    CounterNotSave["Current Bounty"] = lp.leaderstats["Bounty/Honor"].Value
                                    CounterNotSave["Earned"] = CounterNotSave["Current Bounty"] - CounterNotSave["Old Bounty"]
                                    CounterSave["Total Earned"] = CounterNotSave["Old Total Earned"] + CounterNotSave["Earned"]
                                    CrentBounty.Text = "Current Bounty: "..str(CounterNotSave["Current Bounty"]).."$"
                                    BountyEarned.Text = "Bounty Earned: "..str(CounterNotSave["Earned"]).."$"
                                    TotalBountyEarned.Text = "Total Bounty Earned: "..str(CounterSave["Total Earned"]).."$"
                                    if not bounty.hop then
                                        if CounterNotSave["Earned2"] ~= CounterNotSave["Earned"] then
                                            counttick = tick()
                                            wt(0.1)
                                            if counter.Webhook.Enabled and bounty["killed"] then
                                                sendwebhook(bounty["killed"], CounterSave["Total Earned"], str(totaltime(CounterNotSave["Account Time Count"], true)))
                                            end
                                            CounterNotSave["Earned2"] = CounterNotSave["Earned"]
                                        end
                                        CilentTimeElapsed.Text = "Cilent Time Elapsed: "..str(totaltime(CounterNotSave["Cilent Time Count"], false))
                                        AccountTimeElapsed.Text = "Account Time Elapsed: "..str(totaltime(CounterNotSave["Account Time Count"], true))
                                    end
                                    save("counter", CounterSave)
                                    wt(0.5)
                                end
                            end)
                        end
                    }

                    if config["Misc"]["Showcase Mode"] then
                        TweenService:Create(
                            MainFrame, 
                            TweenInfo.new(0.8), 
                            {Position = UDim2.new(0.8, 0, 0.8, 0)}
                        ):Play()
                    end

                    MakeDraggable(MainFrame, MainFrame)
                else
                    local BountyGui = Instance.new("ScreenGui");
                    local Main = Instance.new("Frame");
                    local MainFrame = Instance.new("Frame");
                    local NameHub = Instance.new("TextLabel");
                    local ABTText = Instance.new("TextLabel");
                    local TotalBountyEarned = Instance.new("TextLabel");
                    local TimeElapsed = Instance.new("TextLabel");
                    local WTFCLJV = Instance.new("TextLabel");
                    local WTFCLJV1 = Instance.new("TextLabel");
                    local WTFCLJV2 = Instance.new("TextLabel");
                    
                    BountyGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
                    BountyGui.Name = "BountyGui"
                    BountyGui.Parent = coregui
                    
                    Main.BackgroundColor3 = Color3.fromRGB(30.00000011175871, 30.00000011175871, 30.00000011175871)
                    Main.BackgroundTransparency = 0.1
                    Main.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    Main.BorderSizePixel = 0
                    Main.Position = UDim2.new(0, 0, 0, -60)
                    Main.Size = UDim2.new(1, 0, 1, 60)
                    Main.Name = "Main"
                    Main.Parent = BountyGui
                    
                    MainFrame.AnchorPoint = Vector2.new(0.5, 0.5)
                    MainFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    MainFrame.BackgroundTransparency = 0.9990000128746033
                    MainFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    MainFrame.BorderSizePixel = 0
                    MainFrame.Position = UDim2.new(0.5, 0, 0.5, 0)
                    MainFrame.Size = UDim2.new(0, 600, 0, 400)
                    MainFrame.Name = "MainFrame"
                    MainFrame.Parent = Main
                    
                    NameHub.Font = Enum.Font.GothamMedium
                    NameHub.Text = "Grape Haven"
                    NameHub.TextColor3 = Color3.fromRGB(60.00000022351742, 153.00000607967377, 244.0000006556511)
                    NameHub.TextSize = 45
                    NameHub.TextXAlignment = Enum.TextXAlignment.Right
                    NameHub.AnchorPoint = Vector2.new(0.5, 0)
                    NameHub.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    NameHub.BackgroundTransparency = 0.9990000128746033
                    NameHub.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    NameHub.BorderSizePixel = 0
                    NameHub.Position = UDim2.new(0.5, -115, 0, 90)
                    NameHub.Size = UDim2.new(0, 164, 0, 50)
                    NameHub.Name = "NameHub"
                    NameHub.Parent = MainFrame
                    
                    ABTText.Font = Enum.Font.GothamMedium
                    ABTText.Text = "Auto Bounty"
                    ABTText.TextColor3 = Color3.fromRGB(212.00000256299973, 20.000000707805157, 22.000000588595867)
                    ABTText.TextSize = 40
                    ABTText.TextXAlignment = Enum.TextXAlignment.Left
                    ABTText.AnchorPoint = Vector2.new(0.5, 0)
                    ABTText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    ABTText.BackgroundTransparency = 0.9990000128746033
                    ABTText.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    ABTText.BorderSizePixel = 0
                    ABTText.Position = UDim2.new(0.5, 90, 0, 90)
                    ABTText.Size = UDim2.new(0, 212, 0, 50)
                    ABTText.Name = "ABTText"
                    ABTText.Parent = MainFrame
                    
                    TotalBountyEarned.Font = Enum.Font.GothamMedium
                    TotalBountyEarned.Text = "Total Bounty Earned: 0"
                    TotalBountyEarned.TextColor3 = Color3.fromRGB(97.00000181794167, 94.0000019967556, 97.00000181794167)
                    TotalBountyEarned.TextSize = 33
                    TotalBountyEarned.AnchorPoint = Vector2.new(0.5, 0)
                    TotalBountyEarned.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    TotalBountyEarned.BackgroundTransparency = 0.9990000128746033
                    TotalBountyEarned.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    TotalBountyEarned.BorderSizePixel = 0
                    TotalBountyEarned.Position = UDim2.new(0.5, 0, 0, 140)
                    TotalBountyEarned.Size = UDim2.new(0, 245, 0, 30)
                    TotalBountyEarned.Name = "TotalBountyEarned"
                    TotalBountyEarned.Parent = MainFrame
                    
                    TimeElapsed.Font = Enum.Font.GothamMedium
                    TimeElapsed.Text = "Time Elapsed: 0h:0m:0s"
                    TimeElapsed.TextColor3 = Color3.fromRGB(97.00000181794167, 94.0000019967556, 97.00000181794167)
                    TimeElapsed.TextSize = 33
                    TimeElapsed.AnchorPoint = Vector2.new(0.5, 0)
                    TimeElapsed.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    TimeElapsed.BackgroundTransparency = 0.9990000128746033
                    TimeElapsed.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    TimeElapsed.BorderSizePixel = 0
                    TimeElapsed.Position = UDim2.new(0.5, 0, 0, 170)
                    TimeElapsed.Size = UDim2.new(0, 245, 0, 35)
                    TimeElapsed.Name = "TimeElapsed"
                    TimeElapsed.Parent = MainFrame
                    
                    WTFCLJV.Font = Enum.Font.GothamMedium
                    WTFCLJV.Text = "God saw the light, that is good:"
                    WTFCLJV.TextColor3 = Color3.fromRGB(97.00000181794167, 94.0000019967556, 97.00000181794167)
                    WTFCLJV.TextSize = 27
                    WTFCLJV.AnchorPoint = Vector2.new(0.5, 0)
                    WTFCLJV.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    WTFCLJV.BackgroundTransparency = 0.9990000128746033
                    WTFCLJV.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    WTFCLJV.BorderSizePixel = 0
                    WTFCLJV.Position = UDim2.new(0.5, 0, 0, 205)
                    WTFCLJV.Size = UDim2.new(0, 245, 0, 27)
                    WTFCLJV.Name = "WTFCLJV"
                    WTFCLJV.Parent = MainFrame
                    
                    WTFCLJV1.Font = Enum.Font.GothamMedium
                    WTFCLJV1.Text = "and God divided the light from the darkness. 5And God called the light Day,"
                    WTFCLJV1.TextColor3 = Color3.fromRGB(97.00000181794167, 94.0000019967556, 97.00000181794167)
                    WTFCLJV1.TextSize = 27
                    WTFCLJV1.AnchorPoint = Vector2.new(0.5, 0)
                    WTFCLJV1.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    WTFCLJV1.BackgroundTransparency = 0.9990000128746033
                    WTFCLJV1.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    WTFCLJV1.BorderSizePixel = 0
                    WTFCLJV1.Position = UDim2.new(0.5, 0, 0, 232)
                    WTFCLJV1.Size = UDim2.new(0, 245, 0, 27)
                    WTFCLJV1.Name = "WTFCLJV"
                    WTFCLJV1.Parent = MainFrame
                    
                    WTFCLJV2.Font = Enum.Font.GothamMedium
                    WTFCLJV2.Text = "and the darkness he called Night. And there was evening and there was morning, one day"
                    WTFCLJV2.TextColor3 = Color3.fromRGB(97.00000181794167, 94.0000019967556, 97.00000181794167)
                    WTFCLJV2.TextSize = 27
                    WTFCLJV2.AnchorPoint = Vector2.new(0.5, 0)
                    WTFCLJV2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    WTFCLJV2.BackgroundTransparency = 0.9990000128746033
                    WTFCLJV2.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    WTFCLJV2.BorderSizePixel = 0
                    WTFCLJV2.Position = UDim2.new(0.5, 0, 0, 259)
                    WTFCLJV2.Size = UDim2.new(0, 245, 0, 27)
                    WTFCLJV2.Name = "WTFCLJV"
                    WTFCLJV2.Parent = MainFrame

                    func {
                        bounty.functions.main,
                        "main counter",
                        function()
                            spawn(function()
                                while wt() do
                                    CounterNotSave["Current Bounty"] = lp.leaderstats["Bounty/Honor"].Value
                                    CounterNotSave["Earned"] = CounterNotSave["Current Bounty"] - CounterNotSave["Old Bounty"]
                                    CounterSave["Total Earned"] = CounterNotSave["Old Total Earned"] + CounterNotSave["Earned"]
                                    TotalBountyEarned.Text = "Total Bounty Earned: "..str(CounterSave["Total Earned"])
                                    if not bounty.hop then
                                        if CounterNotSave["Earned2"] ~= CounterNotSave["Earned"] then
                                            counttick = tick()
                                            wt(0.1)
                                            if counter.Webhook.Enabled and bounty["killed"] then
                                                oldsend = bounty["killed"]
                                                sendwebhook(bounty["killed"], CounterSave["Total Earned"], str(totaltime(CounterNotSave["Account Time Count"], true)))
                                            end
                                            CounterNotSave["Earned2"] = CounterNotSave["Earned"]
                                        end
                                        TimeElapsed.Text = "Time Elapsed: "..str(totaltime(CounterNotSave["Account Time Count"], true))
                                    end
                                    save("counter", CounterSave)
                                    wt(0.5)
                                end
                            end)
                        end
                    }

                    local SkipGui = Instance.new("ScreenGui");
                    local SkipFrame = Instance.new("Frame");
                    local UICorner = Instance.new("UICorner");
                    local DropShadowHolder = Instance.new("Frame");
                    local DropShadow = Instance.new("ImageLabel");
                    local SkipButton = Instance.new("TextButton");
                    local RuaImage = Instance.new("ImageLabel");
                    local RuaTilte = Instance.new("TextLabel");
                    local RuaContent = Instance.new("TextLabel");
                    
                    SkipGui.DisplayOrder = 10
                    SkipGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
                    SkipGui.Name = "SkipGui"
                    SkipGui.Parent = coregui
                
                    SkipFrame.AnchorPoint = Vector2.new(0.5, 0)
                    SkipFrame.BackgroundColor3 = Color3.fromRGB(50.00000011175871, 50.00000011175871, 50.00000011175871)
                    SkipFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    SkipFrame.BorderSizePixel = 0
                    SkipFrame.Position = UDim2.new(0.5, 0, 0, 50)
                    SkipFrame.Size = UDim2.new(0, 185, 0, 50)
                    SkipFrame.Name = "SkipFrame"
                    SkipFrame.Parent = SkipGui
                
                    UICorner.CornerRadius = UDim.new(0, 8)
                    UICorner.Parent = SkipFrame
                
                    DropShadowHolder.BackgroundTransparency = 1
                    DropShadowHolder.BorderSizePixel = 0
                    DropShadowHolder.Size = UDim2.new(1, 0, 1, 0)
                    DropShadowHolder.ZIndex = 0
                    DropShadowHolder.Name = "DropShadowHolder"
                    DropShadowHolder.Parent = SkipFrame
                
                    DropShadow.Image = "rbxassetid://6015897843"
                    DropShadow.ImageColor3 = Color3.fromRGB(50, 50, 50)
                    DropShadow.ImageTransparency = 0.5
                    DropShadow.ScaleType = Enum.ScaleType.Slice
                    DropShadow.SliceCenter = Rect.new(49, 49, 450, 450)
                    DropShadow.AnchorPoint = Vector2.new(0.5, 0.5)
                    DropShadow.BackgroundTransparency = 1
                    DropShadow.BorderSizePixel = 0
                    DropShadow.Position = UDim2.new(0.5, 0, 0.5, 0)
                    DropShadow.Size = UDim2.new(1, 47, 1, 47)
                    DropShadow.ZIndex = 0
                    DropShadow.Name = "DropShadow"
                    DropShadow.Parent = DropShadowHolder
                
                    SkipButton.Font = Enum.Font.SourceSans
                    SkipButton.Text = ""
                    SkipButton.TextColor3 = Color3.fromRGB(0, 0, 0)
                    SkipButton.TextSize = 14
                    SkipButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    SkipButton.BackgroundTransparency = 0.9990000128746033
                    SkipButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    SkipButton.BorderSizePixel = 0
                    SkipButton.Size = UDim2.new(1, 0, 1, 0)
                    SkipButton.Name = "SkipButton"
                    SkipButton.Parent = SkipFrame
                
                    RuaImage.Image = "rbxassetid://18900764695"
                    RuaImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    RuaImage.BackgroundTransparency = 0.9990000128746033
                    RuaImage.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    RuaImage.BorderSizePixel = 0
                    RuaImage.Position = UDim2.new(0, 10, 0, 1)
                    RuaImage.Size = UDim2.new(0, 40, 0, 40)
                    RuaImage.Name = "RuaImage"
                    RuaImage.Parent = SkipFrame
                
                    RuaTilte.FontFace = Font.new("rbxasset://fonts/families/GothamSSm.json", Enum.FontWeight.ExtraBold, Enum.FontStyle.Normal)
                    RuaTilte.Text = "Grape Haven"
                    RuaTilte.TextColor3 = Color3.fromRGB(131.00000739097595, 181.0000044107437, 255)
                    RuaTilte.TextSize = 13
                    RuaTilte.TextXAlignment = Enum.TextXAlignment.Left
                    RuaTilte.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    RuaTilte.BackgroundTransparency = 0.9990000128746033
                    RuaTilte.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    RuaTilte.BorderSizePixel = 0
                    RuaTilte.Position = UDim2.new(0, 61, 0, 12)
                    RuaTilte.Size = UDim2.new(1, -74, 0, 13)
                    RuaTilte.Name = "RuaTilte"
                    RuaTilte.Parent = SkipFrame
                
                    RuaContent.Font = Enum.Font.GothamBold
                    RuaContent.Text = "Click to skip target"
                    RuaContent.TextColor3 = Color3.fromRGB(230.00000149011612, 230.00000149011612, 230.00000149011612)
                    RuaContent.TextSize = 12
                    RuaContent.TextTransparency = 0.699999988079071
                    RuaContent.TextXAlignment = Enum.TextXAlignment.Left
                    RuaContent.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                    RuaContent.BackgroundTransparency = 0.9990000128746033
                    RuaContent.BorderColor3 = Color3.fromRGB(0, 0, 0)
                    RuaContent.BorderSizePixel = 0
                    RuaContent.Position = UDim2.new(0, 61, 0, 25)
                    RuaContent.Size = UDim2.new(1, -74, 0, 11)
                    RuaContent.Name = "RuaContent"
                    RuaContent.Parent = SkipFrame

                    SkipButton.Activated:Connect(function()
                        if bounty.targ then
                            noti("Skiped Player: "..bounty.targ.Name)
                        end
                        targ()
                    end)
                end
            else
                local SkipGui = Instance.new("ScreenGui");
                local SkipFrame = Instance.new("Frame");
                local UICorner = Instance.new("UICorner");
                local DropShadowHolder = Instance.new("Frame");
                local DropShadow = Instance.new("ImageLabel");
                local SkipButton = Instance.new("TextButton");
                local RuaImage = Instance.new("ImageLabel");
                local RuaTilte = Instance.new("TextLabel");
                local RuaContent = Instance.new("TextLabel");
                
                SkipGui.DisplayOrder = 10
                SkipGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
                SkipGui.Name = "SkipGui"
                SkipGui.Parent = coregui
            
                SkipFrame.AnchorPoint = Vector2.new(0.5, 0)
                SkipFrame.BackgroundColor3 = Color3.fromRGB(30.00000011175871, 30.00000011175871, 30.00000011175871)
                SkipFrame.BorderColor3 = Color3.fromRGB(0, 0, 0)
                SkipFrame.BorderSizePixel = 0
                SkipFrame.Position = UDim2.new(0.5, 0, 0, 50)
                SkipFrame.Size = UDim2.new(0, 185, 0, 50)
                SkipFrame.Name = "SkipFrame"
                SkipFrame.Parent = SkipGui
            
                UICorner.CornerRadius = UDim.new(0, 8)
                UICorner.Parent = SkipFrame
            
                DropShadowHolder.BackgroundTransparency = 1
                DropShadowHolder.BorderSizePixel = 0
                DropShadowHolder.Size = UDim2.new(1, 0, 1, 0)
                DropShadowHolder.ZIndex = 0
                DropShadowHolder.Name = "DropShadowHolder"
                DropShadowHolder.Parent = SkipFrame
            
                DropShadow.Image = "rbxassetid://6015897843"
                DropShadow.ImageColor3 = Color3.fromRGB(30, 30, 30)
                DropShadow.ImageTransparency = 0.5
                DropShadow.ScaleType = Enum.ScaleType.Slice
                DropShadow.SliceCenter = Rect.new(49, 49, 450, 450)
                DropShadow.AnchorPoint = Vector2.new(0.5, 0.5)
                DropShadow.BackgroundTransparency = 1
                DropShadow.BorderSizePixel = 0
                DropShadow.Position = UDim2.new(0.5, 0, 0.5, 0)
                DropShadow.Size = UDim2.new(1, 47, 1, 47)
                DropShadow.ZIndex = 0
                DropShadow.Name = "DropShadow"
                DropShadow.Parent = DropShadowHolder
            
                SkipButton.Font = Enum.Font.SourceSans
                SkipButton.Text = ""
                SkipButton.TextColor3 = Color3.fromRGB(0, 0, 0)
                SkipButton.TextSize = 14
                SkipButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                SkipButton.BackgroundTransparency = 0.9990000128746033
                SkipButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
                SkipButton.BorderSizePixel = 0
                SkipButton.Size = UDim2.new(1, 0, 1, 0)
                SkipButton.Name = "SkipButton"
                SkipButton.Parent = SkipFrame
            
                RuaImage.Image = "rbxassetid://18900764695"
                RuaImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                RuaImage.BackgroundTransparency = 0.9990000128746033
                RuaImage.BorderColor3 = Color3.fromRGB(0, 0, 0)
                RuaImage.BorderSizePixel = 0
                RuaImage.Position = UDim2.new(0, 10, 0, 1)
                RuaImage.Size = UDim2.new(0, 40, 0, 40)
                RuaImage.Name = "RuaImage"
                RuaImage.Parent = SkipFrame
            
                RuaTilte.FontFace = Font.new("rbxasset://fonts/families/GothamSSm.json", Enum.FontWeight.ExtraBold, Enum.FontStyle.Normal)
                RuaTilte.Text = "Grape Haven"
                RuaTilte.TextColor3 = Color3.fromRGB(131.00000739097595, 181.0000044107437, 255)
                RuaTilte.TextSize = 13
                RuaTilte.TextXAlignment = Enum.TextXAlignment.Left
                RuaTilte.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                RuaTilte.BackgroundTransparency = 0.9990000128746033
                RuaTilte.BorderColor3 = Color3.fromRGB(0, 0, 0)
                RuaTilte.BorderSizePixel = 0
                RuaTilte.Position = UDim2.new(0, 61, 0, 12)
                RuaTilte.Size = UDim2.new(1, -74, 0, 13)
                RuaTilte.Name = "RuaTilte"
                RuaTilte.Parent = SkipFrame
            
                RuaContent.Font = Enum.Font.GothamBold
                RuaContent.Text = "Click to skip target"
                RuaContent.TextColor3 = Color3.fromRGB(230.00000149011612, 230.00000149011612, 230.00000149011612)
                RuaContent.TextSize = 12
                RuaContent.TextTransparency = 0.699999988079071
                RuaContent.TextXAlignment = Enum.TextXAlignment.Left
                RuaContent.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                RuaContent.BackgroundTransparency = 0.9990000128746033
                RuaContent.BorderColor3 = Color3.fromRGB(0, 0, 0)
                RuaContent.BorderSizePixel = 0
                RuaContent.Position = UDim2.new(0, 61, 0, 25)
                RuaContent.Size = UDim2.new(1, -74, 0, 11)
                RuaContent.Name = "RuaContent"
                RuaContent.Parent = SkipFrame

                SkipButton.Activated:Connect(function()
                    if bounty.targ then
                        noti("Skiped Player: "..bounty.targ.Name)
                    end
                    targ()
                end)    
            end

            fl {
                bounty.functions.main,
                function(i, v)
                    v()
                end
            }
        end)
        task.wait(5)
        if not scriptloaded then
            game:GetService("TeleportService"):Teleport(game.PlaceId, lp)
        end
    else
        game.Players.LocalPlayer:Kick(check.bountyhavekey.kick)
    end