getgenv().EquipMacroTroop = true
getgenv().ImportMacro = "https://cdn.discordapp.com/attachments/1247370643796660296/1247763035565850645/message.txt?ex=66613561&is=665fe3e1&hm=124720515cccbd3c9454255f001c5e6ef64dab9850f1e8b4b3cd995ae0edab99&"
getgenv().Config = {
	["AutoSave"] = true,
	["WH_MatchComplete"] = true,
	["AutoSkip"] = true,
	["AutoClaimQuest"] = true,
	["TPLobbyIfPlayer"] = false,
	["AutoJoinHoster"] = false,
	["WaitForJoiner"] = false,
	["SelectMacro"] = "ice",
	["JoinerList"] = {
		[1] = "No joiner in the list"
	},
	["AutoSummonStandard"] = false,
	["SelectReward"] = {
		["Trait Crystal"] = false,
		["Risky Dice"] = false,
		["Star Rift (Rainbow)"] = false,
		["Gems"] = false,
		["Star Rift "] = false,
		["Gold"] = false
	},
	["BoostFPS"] = true,
	["SelectDifficulty"] = {
		["All units lowered range"] = false,
		["All enemies regen health"] = false,
		["All enemies +1 shield"] = false,
		["All units increased cost"] = false,
		["All enemies 2x health"] = false,
		["All enemies 2x speed"] = false
	},
	["JoinerCooldown"] = 0,
	["UnselectIfSummoned"] = false,
	["AutoReplay"] = true,
	["AutoReturnLobby"] = false,
	["AutoLeave"] = false,
	["AutoJoinRaid"] = false,
	["SelectChallengeWorld"] = {
		["Cursed Academy"] = false,
		["Blue Planet"] = false,
		["Windmill Village"] = false,
		["Mob City"] = false,
		["Underwater Temple"] = false
	},
	["HardMode"] = false,
	["WH_BannerSummoned"] = true,
	["SelectChallengeMacro"] = {

	},
	["SelectWorld"] = "Windmill Village",
	["AutoJoinWorld"] = true,
	["AutoJoinChallenge"] = false,
	["LimitedSummonUnit"] = {
		["Donut Warrior"] = false,
		["The Cursed Knight"] = false,
		["Ocean Guardian"] = false,
		["Warrior Princess"] = false,
		["Electric Cyborg"] = false
	},
	["StandardSummonUnit"] = {
		["Dragon Slayer"] = false,
		["Shinobi Form 3"] = false,
		["The Gamer"] = false,
		["Carp"] = false,
		["Admiral Of Lava"] = false,
		["Bloomer"] = false,
		["The Beast"] = false,
		["Admiral Of Light"] = false,
		["Pink Rockstar"] = false,
		["Clay"] = false,
		["Cursed Fighter"] = false,
		["Ascended Qi Master"] = false,
		["Fire Leg Master"] = false,
		["Esper"] = false,
		["Maxed Qi Master"] = false,
		["Flame Dragon King"] = false,
		["Thunder Shinobi"] = false,
		["Curse Prince"] = false,
		["Spirit Hybrid"] = false,
		["Soulforce Reaper"] = false,
		["Admiral Of Ice"] = false,
		["Inferno Commander"] = false,
		["Elf Wizardess"] = false,
		["Ant King"] = false,
		["Sharpshooter"] = false,
		["Skull Warrior"] = false,
		["Vengeful Shinobi"] = false,
		["Strongest Swordsman"] = false,
		["Chance Taker"] = false,
		["Master Swordsman"] = false
	},
	["BlackScreen"] = true,
	["JoinHighest"] = true,
	["AutoSell"] = false,
	["AutoRejoin"] = true,
	["WebhookURL"] = "https://discord.com/api/webhooks/1213393127608557629/RQk17TR696Hp0fFbBZERigntjaGNLX_JNuDtlFNglzc8w0u5YeyFk9vKAgOJnNWVmzFu",
	["LeaveSellWave"] = 1,
	["PlayMacro"] = true,
	["DeleteRarity"] = {
		["Epic"] = false,
		["Legendary"] = false,
		["Rare"] = false
	},
	["IgnoreMacroTiming"] = true,
	["SelectRaidMacro"] = {

	},
	["AutoSummonLimited"] = false
}
getgenv().Key = "kff5c12f0023fd7aecf7e96a"
loadstring(game:HttpGet("https://nousigi.com/loader.lua"))() 