repeat task.wait() until game:IsLoaded()
repeat task.wait() until game.Players
repeat task.wait() until game.Players.LocalPlayer
repeat task.wait() until game.Players.LocalPlayer:FindFirstChild("PlayerGui")
repeat task.wait() until game.Players.LocalPlayer.PlayerGui:FindFirstChild("Main");
_G.Team = "Pirate" -- Marine / Pirate
getgenv().Script_Mode = "Kaitun_Script"
_G.WebHook = {
    ["Enabled"] = true, 
    ["Url"] = "https://discord.com/api/webhooks/1298511758511833161/ne80KYG2KzbKg4CmUv5yYpQA-bHShMWD1ZUTHSGC8g0T0y1th6-w2o76yAnpX6U5DQKG", -- ลิ้งค์เว็บฮุก
    ["Delay"] = 60 
}
_G.MainSettings = {
        ["EnabledHOP"] = true,
        ['FPSBOOST'] = true, 
        ["FPSLOCKAMOUNT"] = 15,
        ['WhiteScreen'] = true, 
        ['CloseUI'] = true, 
        ["NotifycationExPRemove"] = true, 
        ['AFKCheck'] = 150, 
        ["LockFragments"] = 20000, 
        ["LockFruitsRaid"] = { 
            [1] = "Dough-Dough",
            [2] = "Dragon-Dragon"
        }
    }
_G.Fruits_Settings = { 
    ['Main_Fruits'] = {'Dough-Dough'}, 
    ['Select_Fruits'] = {"Flame-Flame", "Ice-Ice", "Quake-Quake", "Light-Light", "Dark-Dark", "Spider-Spider", "Rumble-Rumble", "Magma-Magma", "Buddha-Buddha"} 
}
_G.Quests_Settings = { 
    ['Rainbow_Haki'] = true,
    ["MusketeerHat"] = true,
    ["PullLever"] = true,
    ['DoughQuests_Mirror'] = {
        ['Enabled'] = true,
        ['UseFruits'] = true
    }        
}
_G.Races_Settings = { 
    ['Race'] = {
        ['EnabledEvo'] = true,
        ["v2"] = true,
        ["v3"] = true,
        ["Races_Lock"] = {
            ["Races"] = { 
                ["Mink"] = true,
                ["Human"] = true,
                ["Fishman"] = true
            },
            ["RerollsWhenFragments"] = 20000 
        }
    }
}
_G.Settings_Melee = { 
    ['Superhuman'] = true,
    ['DeathStep'] = true,
    ['SharkmanKarate'] = true,
    ['ElectricClaw'] = true,
    ['DragonTalon'] = true,
    ['Godhuman'] = true
}
_G.FarmMastery_Settings = {
    ['Melee'] = true,
    ['Sword'] = true,
    ['DevilFruits'] = true,
    ['Select_Swords'] = {
        ["AutoSettings"] = true,
        ["ManualSettings"] = { 
            "Saber",
            "Buddy Sword"
        }
    }
}
_G.SwordSettings = {
    ['Saber'] = true,
    ["Pole"] = true,
    ['MidnightBlade'] = true,
    ['Shisui'] = true,
    ['Saddi'] = true,
    ['Wando'] = true,
    ['Yama'] = true,
    ['Rengoku'] = true,
    ['Canvander'] = true,
    ['BuddySword'] = true,
    ['TwinHooks'] = true,
    ['HallowScryte'] = true,
    ['TrueTripleKatana'] = true,
    ['CursedDualKatana'] = true
}
_G.SharkAnchor_Settings = {
    ["Enabled_Farm"] = false,
    ['FarmAfterMoney'] = 2500000
}
_G.GunSettings = { 
    ['Kabucha'] = true,
    ['SerpentBow'] = true,
    ['SoulGuitar'] = true
}
getgenv().Key = "MARU-EBSQ-ED6MV-4WKD-OV9BU-3B1Q"
getgenv().id = "1111667456579219487"
getgenv().Script_Mode = "Kaitun_Script"
loadstring(game:HttpGet("https://raw.githubusercontent.com/xshiba/MaruBitkub/main/Mobile.lua"))()